<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/single/title.php'); ?>

<?php
global $inspiry_options;
global $inspiry_single_property;
?>
<div class="single-property-wrapper">
    <header class="entry-header single-property-header">
        <?php
//         if ( $inspiry_options[ 'inspiry_property_header_variation' ] == 2 || $inspiry_options[ 'inspiry_property_header_variation' ] == 3 ) {
//             get_template_part( 'partials/property/single/favorite-and-print' );
//         }
        ?>
        <h1 class="entry-title single-property-title"><?php the_title(); ?></h1>
        <!--sse--><span class="single-property-price price"><?php price_format($inspiry_single_property->price()); ?></span><!--/sse-->
        <div class="print-only" style="display: none;">
            <p><strong>Price:</strong> <?php price_format($inspiry_single_property->price()); ?></p>
        </div>
    </header>
    <?php inspiry_property_meta( $inspiry_single_property ); ?>
</div>
<?php
// if ( $inspiry_options[ 'inspiry_property_header_variation' ] == 1 ) {
//     get_template_part( 'partials/property/single/favorite-and-print' );
// }