<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/single/share.php'); ?>

<?php
global $inspiry_single_property;
if ( function_exists( 'ire_single_property_social_share' ) ) {
	?>
	<div class="row">
		<div class="col-sm-12">
			<div class="property-share-networks clearfix">
				<?php
				global $inspiry_options;
				if ( !empty( $inspiry_options[ 'inspiry_property_share_title' ] ) ) {
					?><h4 class="fancy-title"><?php echo esc_html( $inspiry_options[ 'inspiry_property_share_title' ] ); ?></h4><?php
				}
				?>
				<div class="shared-box">
					<img style="width: 100%; max-height: 50px;" src="/mediacode/images/LargeHorizontalEmpty.gif" data-media="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($inspiry_single_property->get_post_ID()), 'full', false )[0] ?>" data_title="<?php echo get_the_title($inspiry_single_property->get_post_ID()) ?>" data-summary="<?php echo wp_strip_all_tags(get_the_content(null, true, $inspiry_single_property->get_post_ID())) ?>"/>
				</div>
				<?php
				/*
				if ( function_exists( 'sti_sharing_buttons' ) ) {
				    sti_sharing_buttons(true, array('image' => the_post_thumbnail('large'), 'title' => get_the_title(), 'description' => the_post()));
				}
				*/
				// do_shortcode('&#91;sti_image image="/mediacode/images/LargeHorizontalEmpty.gif" shared_image="'. the_post_thumbnail('large') .'" shared_title="' . get_the_title() . '" shared_desc="' . the_post() . '"]');
				// ire_single_property_social_share();
				?>
			</div>
		</div>
	</div>
	<?php
}
?>