<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/members/modal-login.php'); ?>

<div id="login-modal" class="modal fade" tabindex="-1" role="dialog"
	aria-labelledby="login-modal" aria-hidden="true">

	<div class="modal-dialog">
		<div class="modal-content">

			<div class="login-section modal-section">
            <?php get_template_part( 'partials/members/login' ); ?>
        </div>
			<!-- .login-section -->

			<div class="password-section modal-section">
            <?php get_template_part( 'partials/members/reset-password' ); ?>
        </div>
			<!-- .password-reset-section -->

			<div class="register-section modal-section">
            <?php get_template_part( 'partials/members/register' ); ?>
        </div>
			<!-- .register-section -->

		</div>
		<!-- .modal-dialog -->
	</div>

</div>
<!-- .modal -->