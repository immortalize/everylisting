

<?php 

require_once('../../../../../wp-load.php');
foreach ($_GET as $key => $val) {
    if (!startsWith(urldecode($key), "user_role_")) {
        if (urldecode($val) == "undefined") {
            delete_user_meta(get_current_user_id(), urldecode($key));
        } else {
            update_user_meta(get_current_user_id(), urldecode($key), urldecode($val));
        }
    } else {
        $user = wp_get_current_user();
        if ((urldecode($val) == "undefined") || (urldecode($val) == "false")) {
            if (in_array(substr(urldecode($key), strlen("user_role_")), (array)$user->roles)) $user->remove_role(substr(urldecode($key), strlen("user_role_")));
        } else {
            if (!in_array(substr(urldecode($key), strlen("user_role_")), (array)$user->roles)) $user->add_role(substr(urldecode($key), strlen("user_role_")));
        }
    }
}
echo "OK";
?>
