<?php
/*
 * This file contains cluster cache support functions
 */

// To run locally: http://everylisting.localhost:8080/MapMarkerServlet-0.0.1/Process?request=status
if (!function_exists('mapServletUrlPrefix')):
function mapServletUrlPrefix($iptoset) {
    $site = site_url('/', 'https');
    if (startsWith($site, "https://everylisting.localhost/")) {
        $modifiedSite = 'http://' . MAPS_IP_SERVLET . ':8080/MapMarkerServlet/Process?';
        ensureServletDeployed('http://' . MAPS_IP_SERVLET . '/dbDataAndSetUp/MapMarkerServletDeploy.properties.txt', $modifiedSite);
        return $modifiedSite;
    } else {
        if ($iptoset) {
            return 'https://' . MAPS_IP_SERVLET_WRITE . '/MapMarkerServlet/Process?';
        } else {
            return 'https://' . MAPS_IP_SERVLET . '/MapMarkerServlet/Process?';
        }
    }
}
endif;

if (!function_exists('mapServletUrlSuffix')):
function mapServletUrlSuffix() {
    $site = site_url('', 'https');
    if (startsWith($site, "https://everylisting.localhost")) {
        $path = parse_url('http://everylisting.localhost/maps/mapmarkerindexes', PHP_URL_PATH);
        return "&data=" . urlencode($_SERVER['DOCUMENT_ROOT'] . $path);
    } else {
        return "";
    }
}
endif;

if (!function_exists('ensureServletDeployed')):
function ensureServletDeployed($url, $servleturl) {
    $result = getURLContent($url);
    if ($result === FALSE) {
        return false;
    } else {
        $array = array();
        parse_str(str_replace("\n", "&", $result), $array);
        $mustDeploy = false;
        if (!isset($array['MapMarkerServletSourcePath'])) {
            $path = parse_url('http://everylisting.localhost/maps/MapMarkerServlet.war', PHP_URL_PATH);
            $array['MapMarkerServletSourcePath'] = $_SERVER['DOCUMENT_ROOT'] . $path;
        }
        if ((isset($array['MapMarkerServletDestinationPath'])) && (isset($array['MapMarkerServletSourcePath']))) {
            $array['MapMarkerServletDestinationPath'] = str_replace("\\", "/", $array['MapMarkerServletDestinationPath']);
            $array['MapMarkerServletDestinationPath'] = str_replace("\n", "", $array['MapMarkerServletDestinationPath']);
            $array['MapMarkerServletDestinationPath'] = str_replace("\r", "", $array['MapMarkerServletDestinationPath']);
            $array['MapMarkerServletSourcePath'] = str_replace("\\", "/", $array['MapMarkerServletSourcePath']);
            $array['MapMarkerServletSourcePath'] = str_replace("\n", "", $array['MapMarkerServletSourcePath']);
            $array['MapMarkerServletSourcePath'] = str_replace("\r", "", $array['MapMarkerServletSourcePath']);
            $size = 0;
            $mtime = 0;
            if (file_exists($array['MapMarkerServletSourcePath'])) {
                $size = filesize($array['MapMarkerServletSourcePath']);
                $mtime = filemtime($array['MapMarkerServletSourcePath']);
            }
            if (file_exists($array['MapMarkerServletDestinationPath'])) {
                $mustDeploy = ((filesize($array['MapMarkerServletDestinationPath']) != $size) || (filemtime($array['MapMarkerServletDestinationPath']) != $mtime));
            } else {
                $mustDeploy = true;
            }
            if ($mustDeploy) {
                if (!copy($array['MapMarkerServletSourcePath'], $array['MapMarkerServletDestinationPath'])) {
                    echo "ERROR: A deployment of " . $array['MapMarkerServletSourcePath'] . " is needed.";
                    return false;
                } else {
                    $servleturl .= 'request=isok&notimingreport=true';
                    $retries = 0;
                    do {
                        if ($retries > 0) {
                            sleep(2);
                        }
                        $retries++;
                        $result = getURLContent($servleturl);
                    } while (($retries <= 30) && ($result !== FALSE) && (strpos($result, "HTTP Status 404") !== FALSE));
                    if (!startsWith($result, 'OK')) {
                        echo 'ERROR: Unable to deploy ' . $array['MapMarkerServletSourcePath'] . '. You will need to deploy manually.';
                    }
                    return startsWith($result, 'OK');
                }
            }
        }
        return true;
    }
}
endif;

if (!function_exists('cluster_cache_info')):
function cluster_cache_info() {
    return trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=status" . mapServletUrlSuffix(), false));
}
endif;

if (!function_exists('cluster_cache_reset')):
function cluster_cache_reset($setupAlso) {
    if ($setupAlso) {
        $dReturn = trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=setup&countriesimportsetup=true" . mapServletUrlSuffix(), false));
    } else {
        $dReturn = trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=reset&pwd=hwsdfgsfd" . mapServletUrlSuffix(), false));
    }
    return $dReturn;
}
endif;

if (!function_exists('cluster_cache_json_location')):
function cluster_cache_json_location() {
    $dReturn = trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=jsonlocation&nocleanup=true&notimingreport=true" . mapServletUrlSuffix(), false));
    $dReturn = str_replace("\\", "/", $dReturn);
    $dReturn = str_replace("\n", "", $dReturn);
    $dReturn = str_replace("\r", "", $dReturn);
    return $dReturn;
}
endif;

if (!function_exists('cluster_cache_available')):
function cluster_cache_available($verbose) {
    $output = explode("\n", getMapServletResponse(mapServletUrlPrefix(false) . "request=status" . mapServletUrlSuffix(), false));
    foreach ($output as $line) {
        if ($verbose) {
            echo("$line\n");
        }
        if (strcmp($line, "OK") == 0) {
            $ret = "OK";
        }
    }
    return trim($ret);
}
endif;

if (!function_exists('cluster_cache_daemon_running')):
function cluster_cache_daemon_running($verbose) {
    return false;
}
endif;

if (!function_exists('ensure_daemon_running')):
function ensure_daemon_running($verbose) {
    return false;
}
endif;

if (!function_exists('cluster_cache_update_property')):
function cluster_cache_update_property($propertyId, $latitude, $longitude, $verbose, $country, $listingkey) {
    if (wp_count_posts('property')->publish <= 1) {
        $resultfile = 'last_update_map_point.dat';
        if (!file_exists($resultfile)) {
            $result = time();
            file_put_contents($resultfile, serialize($result));
            unset($result);
        } else {
            $result = unserialize(file_get_contents($resultfile));
        }
        if (!isset($result) || ((time() - $result) > 60)) {
            echo (' RESET ' . (isset($result)?('(' . (time() - $result) . ') '):''));
            cluster_cache_reset(true);
        }
        $result = time();
        file_put_contents($resultfile, serialize($result));
    }
    $ret = "ERROR";
    if ((isset($propertyId)) && (isset($country)) && (isset($latitude)) && (isset($longitude))) {
        $output = explode("\n", getMapServletResponse(mapServletUrlPrefix(true) . "request=set&nocleanup=true&country='". urlencode($country) ."'&id=".$propertyId."&lat=".$latitude."&lng=".$longitude . mapServletUrlSuffix(), false));
        foreach ($output as $line) {
            if ($verbose) {
                echo("$line\n");
            }
            if (strcmp($line, "OK") == 0) {
                $ret = $line;
            }
        }
        if ((strcmp($ret, "OK") != 0) && (strlen($ret) > 0)) {
            echo("Error on processing ".$listingkey." at ".$latitude.", ".$longitude.": ".$ret.". \n");
            if (!$verbose) {
                foreach ($output as $line) {
                    echo("$line\n");
                }
            }
        }
    } else if ($verbose) {
        echo("Not all required variables set: propertyId -> $propertyId, latitude -> $latitude, longitude -> $longitude\n");
    }
    return trim($ret);
}
endif;

if (!function_exists('cluster_cache_create_ref_file')):
function cluster_cache_create_ref_file($array, $expiry) {
    $ids = '';
    foreach ($array as $element) {
        if ($ids != '') {
            $ids .= ',';
        }
        $ids .= $element;
    }
    return trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=createreffileforids&nocleanup=true&ids='".$ids."'&notimingreport=true&expiry=" . $expiry . mapServletUrlSuffix(), false));
}
endif;

if (!function_exists('cluster_cache_ids_bounds')):
function cluster_cache_ids_bounds($file) {
    return trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=getidsbounds&nocleanup=true&ref='".$file."'&notimingreport=true" . mapServletUrlSuffix(), false));
}
endif;

if (!function_exists('cluster_cache_ids')):
function cluster_cache_ids($file) {
    return trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=getids&nocleanup=true&ref='".$file."'&notimingreport=true" . mapServletUrlSuffix(), false));
}
endif;

if (!function_exists('cluster_cache_setup')):
function cluster_cache_setup($country, $west, $east, $north, $south) {
    return trim(getMapServletResponse(mapServletUrlPrefix(true) . "request=setup&notimingreport=true" .((isset($country))?("&country=".urlencode($country)):"") .((isset($west))?("&west=".$west):"").((isset($north))?("&north=".$north):"").((isset($east))?("&east=".$east):"").((isset($south))?("&south=".$south):"") . mapServletUrlSuffix(), false));
}
endif;

if (!function_exists('cluster_cache_global_json')):
function cluster_cache_global_json($tolerance, $maxIds, $west, $east, $north, $south, $dcline, $extra, $ids, $zoom, $country, $devicescreentype) {
    if ((isset($country) && ($country != null)) || ((isset($west) && ($west != null)) && (isset($east) && ($east != null)) && (isset($north) && ($north != null)) && (isset($south) && ($south != null)))) {
        $servletCommand = trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=get&notimingreport=true&nocleanup=true" .((isset($maxIds) && ($maxIds != null))?("&maxIds=".$maxIds):"").((isset($dcline) && ($dcline != null))?("&includesDCLine=".$dcline):"").((isset($west))?("&west=".$west):"").((isset($north))?("&north=".$north):"").((isset($east))?("&east=".$east):"").((isset($south))?("&south=".$south):"").((isset($zoom) && ($zoom != null))?("&zoom=".$zoom):"").(((isset($devicescreentype)) && ($devicescreentype != null) && ($devicescreentype === "computer"))?"&computerscreen=true":"").(((isset($devicescreentype)) && ($devicescreentype != null) && ($devicescreentype === "mobile"))?"&mobilescreen=true":"").(((isset($devicescreentype)) && ($devicescreentype != null) && ($devicescreentype === "tablet"))?"&tabletscreen=true":"") . mapServletUrlSuffix(), false));
        return $servletCommand;
    } else {
        $servletCommand = trim(getMapServletResponse(mapServletUrlPrefix(false) . "request=get&notimingreport=true&countOnCountriesCenter=true&nocleanup=true" .(((isset($devicescreentype)) && ($devicescreentype != null) && ($devicescreentype === "computer"))?"&computerscreen=true":"").(((isset($devicescreentype)) && ($devicescreentype != null) && ($devicescreentype === "mobile"))?"&mobilescreen=true":"").(((isset($devicescreentype)) && ($devicescreentype != null) && ($devicescreentype === "tablet"))?"&tabletscreen=true":"") . mapServletUrlSuffix(), false));
        return $servletCommand;
    }
}
endif;

if (!function_exists('cluster_cache_remove_property')):
function cluster_cache_remove_property($propertyId, $verbose) {
    $ret = "ERROR";
    $output = explode("\n", getMapServletResponse(mapServletUrlPrefix(true) . "request=delete&nocleanup=true&id=" . $propertyId . mapServletUrlSuffix(), false));
    foreach ($output as $line) {
        if ($verbose) {
            echo("$line\n");
        }
        if (strcmp($line, "OK") == 0) {
            $ret = "OK";
        }
    }
    return trim($ret);
}
endif;

if (!function_exists('gen_uuid')):
function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        
        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),
        
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,
        
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,
        
        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
}
endif;

if (!function_exists('getURLContent')):
function getURLContent($url) {
    $rnd = gen_uuid();
    $curlret[$rnd] = curl_init();
    curl_setopt($curlret[$rnd], CURLOPT_URL, $url);
    curl_setopt($curlret[$rnd], CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curlret[$rnd], CURLOPT_HEADER, false);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($curlret[$rnd], CURLOPT_COOKIESESSION, true);
    curl_setopt($curlret[$rnd], CURLOPT_FORBID_REUSE, true);
    curl_setopt($curlret[$rnd], CURLOPT_FRESH_CONNECT, true);
    curl_setopt($curlret[$rnd], CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curlret[$rnd], CURLOPT_RETURNTRANSFER, true);
    if (!$result = curl_exec($curlret[$rnd])) {
        trigger_error(curl_error($curlret[$rnd]));
    }
    $httpcode = curl_getinfo($curlret[$rnd], CURLINFO_HTTP_CODE);
    curl_close($curlret[$rnd]);
    unset($curlret[$rnd]);
    return $result;
}
endif;

if (!function_exists('getMapServletResponse')):
function getMapServletResponse($command, $errorreporting) {
    if ($errorreporting) {
        $keep_display_errors = ini_set('display_errors', 1); // 1-turn on all error reporings 0-turn off all error reporings
        $keep_error_reporting = error_reporting();
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    }
    $rnd = gen_uuid();
    $curlret[$rnd] = curl_init();
    curl_setopt($curlret[$rnd], CURLOPT_URL, $command);
    curl_setopt($curlret[$rnd], CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curlret[$rnd], CURLOPT_HEADER, false);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($curlret[$rnd], CURLOPT_COOKIESESSION, true);
    curl_setopt($curlret[$rnd], CURLOPT_FORBID_REUSE, true);
    curl_setopt($curlret[$rnd], CURLOPT_FRESH_CONNECT, true);
    curl_setopt($curlret[$rnd], CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curlret[$rnd], CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curlret[$rnd]);
    $httpcode = curl_getinfo($curlret[$rnd], CURLINFO_HTTP_CODE);
    curl_close($curlret[$rnd]);
    if ($errorreporting) {
        if ($keep_display_errors != false) {
            ini_set('display_errors', $keep_display_errors);
        }
        error_reporting($keep_error_reporting);
    }
    unset($curlret[$rnd]);
    return trim($result);
}
endif;

if (!function_exists('sendDaemonCommand')):
function sendDaemonCommand($command) {
    
    // Prepare data
    $port = 3232;
    $length = strlen($command);
    
    // Connect to socket
    if (!$sock = fsockopen('localhost', $port, $errNo, $errStr)) {
        return false;
    }
    
    // Write command to socket
    while (true) {
        
        // Try and write data to socket
        $sent = fwrite($sock, $command, $length);
        
        // If it failed, error out
        if ($sent === false) {
            fclose($sock);
            return false;
        }
        
        // If there is data left to send, try again
        if ($sent < $length) {
            $command = substr($command, $sent);
            $length -= $sent;
            continue;
        }
        
        // If we get here the write operation was successful
        break;
        
    }
    
    // Read back from socket and close it
    $out = read_line($sock);
    fclose($sock);
    return $out;
}
endif;

$buffer = '';
if (!function_exists('read_line')):
function read_line($socket) {
    global $buffer;
    while (strpos($buffer, "\n") === false) {
        $buffer .= fread($socket, 1024);
    }
    $lineEnd = strpos($buffer, "\n");
    $line = substr($buffer, 0, $lineEnd-1);
    $buffer = substr($buffer, $lineEnd);
    return $line;
}
endif;
?>