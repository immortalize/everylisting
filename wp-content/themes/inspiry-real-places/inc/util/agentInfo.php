<?php

ini_set('display_errors', 1);

define('WP_USE_THEMES', false);  
require_once('../../../../../wp-load.php');

if (isset($_GET['id'])) {
	global $wpdb;
	$results = $wpdb->get_row("select post_id from $wpdb->postmeta where (meta_value = '".$_GET['id']."') and (meta_key = 'IDXGenerator_realEstateBrokerId')");
	if (!empty($results)) {
		$post_id = $results->post_id;
	} else {
		echo json_encode(array('Status' => false , 'message'=> "Invalid Agent IDXGenerator ID" ));
		die;
	}
}

$the_query = new WP_Query(array('post_type' => 'agent','p'=>$post_id)); 

$agent_data = $the_query->posts;

$agent_data[0]->post_content = htmlentities($agent_data[0]->post_content);

$agent = (array)$agent_data[0];

$agent_meta = get_post_meta($post_id);

foreach ($agent_meta as $key => $value) {
	$agent[$key] = $value[0];
}

$attachments = get_children(array('post_parent' => $post_id,
                        'post_status' => 'inherit',
                        'post_type' => 'attachment',
                        'post_mime_type' => 'image',
                        'order' => 'ASC',
                        'orderby' => 'menu_order ID'));

foreach($attachments as $att_id => $attachment) {
    $gent['images'][] = wp_get_attachment_url($attachment->ID);
}

$taxonomies = get_object_taxonomies('agent');
        
$agent['taxonomies'] = wp_get_post_terms($post_id, $taxonomies);
     
if (isset($_GET['filter'])) {
    preg_match_all("/\"(".$_GET['filter'].")\":[ ]?\"(.*?)\"/", json_encode($agent, JSON_PRETTY_PRINT), $out);
    $split = explode("|", $_GET['filter']);
    foreach ($split as $token) {
        if (is_array($agent[$token])) {
            array_push($out[0], "\"".$token."\": ".json_encode($agent[$token]));
        }
    }
    $copy = $out[0];
    echo "{ ";
    foreach ($out[0] as $one) {
        echo $one;
        if (next($copy )) {
            echo ", ";
        }
    }
    echo " }";
} else {
    echo json_encode($agent);
}

?>


