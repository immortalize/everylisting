<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/inspiry-real-places/inc/util/lat-long-cluster-cache.php');

// Since this is sensitive, let's force a pwd that can only be obtained in the code.
$pwd = 'hwsdfgsfd';
if ((isset($_GET['pwd'])) && ($pwd == $_GET['pwd'])) {
    if (isset($_GET['request'])) {
        $request = $_GET['request'];
    } else {
        $request = 'info';
    }
    $slicecount = 10;
    switch ($request) {
        case 'resaveone': {
            $filelocation = "/usr/local/mapmarkerindexes/wordpresspoints.txt";
            if ($_GET['offset'] == 0) {
                file_put_contents($filelocation, "");
            }
            $args = array(
                'post_type' => 'property',
                'offset'    => ($_GET['offset'] > 0)?$_GET['offset']:null,
                'numberposts' => $slicecount
            );
            $all_posts = get_posts($args);
            if ($all_posts) {
                foreach ($all_posts as $post ) {
                    setup_postdata($post);
                    $id = $post->ID;
                    $meta = get_post_meta($id);
                    $lat = get_post_meta($id, 'property_latitude', true);
                    $lng = get_post_meta($id, 'property_longitude', true);
                    $country = get_post_meta($id, 'IDXGenerator_Country', true);
                    $listingkey = get_post_meta($id, 'REAL_HOMES_property_id', true);
                    file_put_contents($filelocation, $id . ", " . $lat . ", " . $lng. ", " . $country . ", " . $listingkey . "\n", FILE_APPEND);
                }
                wp_reset_postdata();
            } else {
                echo 'ERROR: No post found from offset ' . $_GET['offset'];
            }
        }
        break;
        case 'reset': {
            $count = $count_posts = wp_count_posts('property')->publish;
            ?>
              	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
				<?php
              	echo '<p>There are ' . $count . ' properties</p>';
              	echo '<form method="post" action="exportMapMarkerPoints.php?pwd=' . $pwd . '"><ol>';
              	echo '<li>Database asset acquisition</li>';
              	echo '<ol>';
              	echo '<li>Execute this database command (in postgres): <b><i>SELECT jsonvalue, otherinformation FROM normalizedjsonfield WHERE jsonfield=\'Address.Country.value\' AND otherinformation LIKE \'%Bounds:%\' AND approvedvalue = true</i></b></li>';
              	echo '<li>Copy the results here:</li>';
?><label for="countries">Countries information:</label>
<textarea id="countries" name="countries" rows="10" cols="50"></textarea><?php
              	echo '</ol>';
              	echo '<li>MAP RESET: <a href="'. mapServletUrlPrefix(true) . '&request=reset&pwd=' . $pwd . mapServletUrlSuffix() .'">'. mapServletUrlPrefix(true) . '&request=reset&pwd=' . $pwd . mapServletUrlSuffix() .'</a></li>';
              	echo '<li><input type="submit" name="submit" value="Set-up" id="submit"/> MAP SETUP </li>';
              /*
              	$count = 0;
              	for ($start = 0; $start < $count_posts; $start += $slicecount) {
                  	$count++;
              		echo '<li>MAP IMPORT #' . $count . ': <a href="'. site_url('/wp-content/themes/inspiry-real-places/inc/util/rebuildMapMarkers.php?pwd=' . $pwd . '&request=resave&start=' . $start, 'https') . '">' . site_url('/wp-content/themes/inspiry-real-places/inc/util/rebuildMapMarkers.php?pwd=' . $pwd . '&request=resave&start=' . $start, 'https') . '</a></li>';
                }
                */
              	echo '<li><button id="importButton">Export</button> <span id="mapimporttext">EXPORT POINTS</span> </li>';
              	echo '<div id="importprogressdiv" style="visibility:hidden"><div class="w3-light-grey w3-xlarge" style="width:80%"><div id="progressbar" class="w3-container w3-green" style="width:50%">50%</div></div></div>&nbsp;<label for="offset">Offset:</label> <input id="offset" type="text" value="0">';
                echo '<li>At any time, use this to see the status: <a target="_blank" href="'. mapServletUrlPrefix(true) . '&request=status'. mapServletUrlSuffix() .'">'. mapServletUrlPrefix(true) . '&request=status&nocache-true'. mapServletUrlSuffix() .'</a></li>';
              	echo '</ol></form>';
              ?>
                <script>
                  	function importOnePropertyMarker(propertyIndex) {
                    	setImportProgress(propertyIndex, <?php echo $count; ?>);
                      	var url = "<?php echo site_url('/wp-content/themes/inspiry-real-places/inc/util/exportMapMarkerPoints.php?pwd=' . $pwd . '&request=resaveone', 'https'); ?>&offset=" + propertyIndex;
                      	Http = new XMLHttpRequest();
                      	Http.open("POST", url);
                      	Http.send();
                      	Http.onreadystatechange = function() {
	        				if (this.readyState == 4 && this.status == 200) {
                              	setImportProgress(propertyIndex + <?php echo $slicecount; ?>, <?php echo $count; ?>);
                              	if (propertyIndex < <?php echo ($count - $slicecount); ?>) {
                                    propertyIndex += <?php echo $slicecount ?>;
                                    if (propertyIndex >= (<?php echo $count ?> - 1)) {
                                        propertyIndex = (<?php echo $count ?> - 1);
                                    }
                                    setTimeout(importOnePropertyMarker, 10, propertyIndex);
                                } else {
                                  	setImportProgress(<?php echo $count; ?>, <?php echo $count; ?>);
                                }
                            }
                        }
                    }
                  
                  	function setImportProgress(actual, max) {
                    	var percent = Math.trunc((actual/max) * 100);
                    	document.getElementById("progressbar").style.width = percent + "%";
                      	document.getElementById("progressbar").innerHTML = percent + "%";
                      	document.getElementById("mapimporttext").innerHTML = "EXPORT POINTS: " + actual + " / " + max;
                    }
                  
              		document.getElementById("importButton").onclick = function () {
                		document.getElementById("importprogressdiv").style.visibility = 'visible';
                		document.getElementById("importButton").disabled = true;
                		document.getElementById("offset").disabled = true;
                		document.getElementById("importButton").innerHTML = "Importing";
                      	setImportProgress(0, <?php echo $count; ?>);
                  		importOnePropertyMarker(Number(document.getElementById("offset").value));
                  		return false;
              		};
                </script>
				<?php
            }
            break;
          	case 'info': {
              // echo base_url();
              // echo 'First call ' . site_url();
              // site_url();
            	echo 'First call <a href="'. site_url('/wp-content/themes/inspiry-real-places/inc/util/exportMapMarkerPoints.php?pwd=' . $pwd . '&request=reset', 'https') .'">' . site_url('/wp-content/themes/inspiry-real-places/inc/util/rebuildMapMarkers.php?pwd=' . $pwd . '&request=reset', 'https') . '</a>: this will clear the indexes. Once you make this call, you will have further instructions from the output.';
            }
          	break;
        }
      	/*
  		
        */
	} else {
  		echo "Not authorized: specify pwd!";
	}
?>



