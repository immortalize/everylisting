<?php

if (!function_exists('get_posting_scroll_content')) {
    function get_posting_scroll_content($atts, $user_id) {
        $dReturn = array();
        $dReturn['total_access_count'] = 0;
        $dReturn['total_posted_properties'] = 0;
        $dReturn['total_posted_blogs'] = 0;
        $dReturn['total_posted_publicities'] = 0;
        $dReturn['available_properties_to_post'] = array();
        $dReturn['available_blogs_to_post'] = array();
        $dReturn['available_publicities_to_post'] = array();
        $dReturn['sorted_posts'] = array();
        // Let's grab all properties that have videos for the user
        $posts = new WP_Query(array(
            'numberposts'	=> -1,
            'post_type'		=> 'property',
            'meta_query'	=> array(
                'relation'	=> 'OR',
                array(
                    'key'		=> "postvideo_userid_" . $user_id,
                    'compare'	=> 'EXISTS'
                ),
                array(
                    'key'		=> "postimage_userid_" . $user_id,
                    'compare'	=> 'EXISTS'
                )
            )));
        if ($posts) {
            while ($posts->have_posts()) {
                $posts->the_post();
                $post_meta = get_post_custom(get_the_ID());
                // print_r($post_meta);
                $media_key =  (count($post_meta["postvideo_userid_" . $user_id]) > 0)?$post_meta["postvideo_userid_" . $user_id][0]:$post_meta["postimage_userid_" . $user_id][0];
                if ((isset($media_key)) && ($media_key != '')) {
                    $media_key_id = substr($media_key, 0, strpos($media_key, '.'));
                    if (!((isset($post_meta['post_media_trash_' . $media_key_id])) && ($post_meta['post_media_trash_' . $media_key_id] == true))) {
                        $property_post = array();
                        $property_post['post_id'] = get_the_ID();
                        $property_post['url'] = get_the_permalink(get_the_ID());
                        $property_post['title'] = get_the_title(get_the_ID());
                        $property_post['content'] = wp_strip_all_tags(get_the_content(get_the_ID()));
                        if (isset($post_meta["postimage_userid_" . $user_id])) {
                            $property_post['image'] = "http://" . IMAGES_POST_SERVLET . "/ImageGeneratorServlet/Process?request=get&jobsdir=" . urlencode(IMAGES_JOBS_DIR) . "&payload=" . $post_meta["postimage_userid_" . $user_id][0];
                        }
                        if (isset($post_meta["postvideo_userid_" . $user_id])) {
                            $property_post['video'] = "http://" . IMAGES_POST_SERVLET . "/ImageGeneratorServlet/Process?request=get&jobsdir=" . urlencode(IMAGES_JOBS_DIR) . "&payload=" . $post_meta["postvideo_userid_" . $user_id][0];
                        }
                        $property_post['media_key'] = $media_key_id;
                        array_push($dReturn['available_properties_to_post'], $property_post);
                        array_push($dReturn['sorted_posts'], array('key' => 'available_properties_to_post', 'index' => count($dReturn['available_properties_to_post']) - 1));
                    }
                }
            }
        }
    	wp_reset_postdata();
        return $dReturn;
    }
}
        
if (!function_exists('posting_scrolls_function')) {
    function posting_scrolls_function($atts) {
        ?>
        	<style>
                .draggable {
                    color: #333;
                    cursor: move;
                    position: relative;
                    float: left;
                }   
                .target {
                    margin: 1%;
                    padding: 2%;
                    padding-top: 20px;
                    padding-bottom: 20px;
                    border: 1px dashed #333;
                    border-radius: 6px;
                    background-color: rgba(225, 225, 225,0.4);
                } 
                .dragimage {
                    padding-bottom: 15px;
                    display: block;
                }
                .sti-btn {
                    display: none;
                }
                .img-frame-cap {
                    background-color: #fff;
                    padding: 10px 10px 10px 10px;
                    margin: 0px;
                    border: 1px solid #999;
                    box-shadow: 10px 10px 10px #999;
                    border-radius: 6px;
                }
                .available_properties_to_post {
                    background-color: teal;
                }
        	</style>
        	<script>
                var dragItem;
                var container = document.querySelector("#topcontainer");
                var active = false;
                var currentX;
                var currentY;
                var initialX;
                var initialY;
                var droppedOnTarget;
                var xOffset = 0;
                var yOffset = 0;

                $( document ).ready(function() {
                	addEventListeners();
                });

                function addEventListeners() {
                    xOffset = 0;
                    yOffset = 0;
                    droppedOnTarget = undefined;
                	dragItem;
                    container = document.querySelector("#topcontainer");
                    $('.target').bind("mouseenter", mouseEnter);
                    $('.target').bind("touchenter", mouseEnter);
    				$('.target').bind("mouseleave", mouseLeave);
    				$('.target').bind("touchleave", mouseLeave);
    				$('#topcontainer').bind("mouseleave", dragEndCancel);
    				$('#topcontainer').bind("touchleave", dragEndCancel);
                    container.addEventListener("touchstart", dragStart, false);
                    container.addEventListener("touchend", dragEnd, false);
                    container.addEventListener("touchmove", drag, false);
                    container.addEventListener("mousedown", dragStart, false);
                    container.addEventListener("mouseup", dragEnd, false);
                    container.addEventListener("mousemove", drag, false);
                    $('.image_still').hide();
                }

                function mouseLeave(e) {
                	if (active) {
						$(this).css("border", "1px solid #333");
						$(this).css("cursor", "initial");
						droppedOnTarget = undefined;
                	}
                }

                function mouseEnter(e) {
                	if (active) {
                		$(this).css("border", "5px solid #00ff00");
                		$(this).css("cursor", "pointer");
                		droppedOnTarget = $(this);
                	}
                }

                function dragStart(e) {
                    if (e.type === "touchstart") {
                        initialX = e.touches[0].clientX - xOffset;
                        initialY = e.touches[0].clientY - yOffset;
                        dragItem = $(document.elementFromPoint((e.touches[0].pageX | 0) - window.pageXOffset, (e.touches[0].pageY | 0) - window.pageYOffset))[0];
                    } else {
                        initialX = e.clientX - xOffset;
                        initialY = e.clientY - yOffset;
                        dragItem = $(document.elementFromPoint((e.pageX | 0) - window.pageXOffset, (e.pageY | 0) - window.pageYOffset))[0];
                    }
                    if ((dragItem != undefined) && ($("#" + dragItem.id).hasClass("draggable"))) {
                        if (e.target === dragItem) {
                            active = true;
                            $(document).data("beforedrag", $("#" + container.id).clone(true));
                        }
                        $("#" + dragItem.id).css("cursor", "move");
                        $('.target').css("border", "1px solid #333");
                    } else {
                    	dragItem = undefined;
                        console.log("cancelling");
                    	e.preventDefault();
                    }
                }

                function dragEndCancel(e) {
                    if (active) {
                		dragEnd(e);
                		e.preventDefault();
                    }
                }

                function dragEnd(e) {
                    if (active) {
                        var droppedOnTargetOnEntry = droppedOnTarget;
                    	if ((e.type === "touchend") && (droppedOnTargetOnEntry == undefined)) {
                        	var eleAtPoint = $(document.elementFromPoint((e.changedTouches[0].pageX | 0) - window.pageXOffset, (e.changedTouches[0].pageY | 0) - window.pageYOffset));
                    		if ((eleAtPoint != undefined) && (eleAtPoint[0] != undefined) && (eleAtPoint[0].classList != undefined) && (eleAtPoint[0].classList.contains("target"))) {
                    			droppedOnTargetOnEntry = eleAtPoint;
                    		}
                    	}
                        active = false;
                        $('.target').css("border", "1px dashed #333");
                        $("#" + dragItem.id).css("cursor", "initial"); 
                        $(document).data("beforedrag").replaceAll("#" + container.id);  
                        addEventListeners();
                        if (droppedOnTargetOnEntry != undefined) {
                            setTimeout(dragEvent, 0, $('#' + dragItem.id), droppedOnTargetOnEntry);
                        }
                    }
                }

                function dragEvent(draggable, target) {
                    if (target.attr('id') != 'trash') {
                        console.log('Detected a drag from ' + draggable.attr('id') + ' to ' + target.attr('id') + ": " + '#' + draggable.attr('id') + '_buttons > div > div > div > div.sti-btn.sti-' + target.attr('id').substring('target_'.length) + '-btn');
                    	$('#' + draggable.attr('id') + '_buttons > div > div > div > div.sti-btn.sti-' + target.attr('id').substring('target_'.length) + '-btn').click();
                    } else {
                    	var attr = $('#' + draggable.attr('id')).attr('data-post-id');
                        if (typeof attr !== typeof undefined && attr !== false) {
    						var locHttp = new XMLHttpRequest();
     						locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/savePostMeta.php?id=" + attr + "&post_media_trash_" + $('#' + draggable.attr('id')).attr('data-media-key') + "=true");
     						locHttp.send();
                        }
                    	$('#' + draggable.attr('id')).parent().remove();
                    }
                }

                function drag(e) {
                    if (active) {
                        if (e.type === "touchmove") {
//                             if (e.touches[0].pageY <= ($('html').offset().top + 50)) {
//                                 console.log("scroll up: " + e.touches[0].pageY + " / " + ($('html').offset().top + 50) + " / " + $(window).scrollTop());
//                                 $("#topcontainer").scrollTo(-20);
//                                 initialY += 20;
//                             } else if (e.touches[0].pageY >= $(window).height() - 50) {
//                             	console.log("scroll down: " + e.touches[0].pageY + " / " + ($(window).height() - 50) + " / " + $(window).scrollTop());
//                             	$("#topcontainer").scrollTo(20);
//                             	initialY -= 20;
//                             }
                            currentX = e.touches[0].clientX - initialX;
                            currentY = e.touches[0].clientY - initialY;
                        } else {
                            currentX = e.clientX - initialX;
                            currentY = e.clientY - initialY;
                        }
                        xOffset = currentX;
                        yOffset = currentY;
                        setTranslate(currentX, currentY, dragItem);
                    }
                    e.preventDefault();
                }

                function setTranslate(xPos, yPos, el) {
					el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
                }
            </script>
            <h1>Postings</h1>
            <div id="instructionsalert" class="w-100 alert alert-info">
    		<button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="$('#instructionsalert').hide();"><span aria-hidden="true">&times;</span></button>
            <p class="text-center">Drag <strong>Image</strong> to <strong>Social-Media Icon</strong> to post (including text content)...<br/><img style="height: 60px; width: 80%;" src="/mediacode/images/right-arrow.gif"/></p>
    		</div>
    		<script>
    		
    		</script>
    		<div id="topcontainer" class="container-fluid clearfix" style="overflow-x: hidden; overflow-y: scroll; max-width: 100%; min-width: 100%; width: 100%; height: 100%; min-height: 100%; background-color: white; margin: 0px;">
            	<div class="d-flex w-100">
                	<div class='row w-100'>
                		<div class="d-flex flex-column" style="max-width: 90%; margin-left: 0; margin-right: 0;">
                			<?php 
                	           $socialmedia = get_social_media_array(false);
                	           $socialmediabtns = '';
					           foreach ($socialmedia as $onesocialmedia) {
					               if ($socialmediabtns != '') {
					                   $socialmediabtns .= ',';
					               }
					               $socialmediabtns .= $onesocialmedia['tech_name'];
					           }
					           $post_count = 0;
					           $scroll_content = get_posting_scroll_content($atts, get_current_user_id());
					           // print_r($scroll_content);
					           foreach ($scroll_content['sorted_posts'] as $one_scroll_content) {
					               $post_count++;
					               if (isset($scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['video'])) {
    					               ?>
    					               	<div class="dragimage video_image">
    					               		<?php $image = $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['video']?>
                                			<img id="image<?=$post_count?>" data-media-key="<?=$scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['media_key']?>" data-post-id="<?=$scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['post_id']?>" src="<?=$image?>" class="draggable img-frame-cap <?= $one_scroll_content['key']?>" style="max-width: 90%;" alt="<?= $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['title'] ?>"/>
                            			</div>
                            			<div id="image<?=$post_count?>_buttons" style="display: none;">
                            				<?php 
                            				    if (function_exists('sti_sharing_buttons')) {
                            				        sti_sharing_buttons(true, array('url' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['url'], 'image' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['image'], 'title' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['title'], 'description' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['content'], "buttons" => $socialmediabtns));
                                				}
                                			?>
                            			</div>
                            			<?php
                            			$post_count++;
					               } 
					               if (isset($scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['image'])) {
					                   ?>
    					               	<div class="dragimage image_still">
    					               		<?php $image = $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['image']?>
                                			<img id="image<?=$post_count?>" data-media-key="<?=$scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['media_key']?>" data-post-id="<?=$scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['post_id']?>" src="<?=$image?>" class="draggable img-frame-cap <?= $one_scroll_content['key']?>" style="max-width: 90%;" alt="<?= $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['title'] ?>"/>
                            			</div>
                            			<div id="image<?=$post_count?>_buttons" style="display: none;">
                            				<?php 
                            				    if (function_exists('sti_sharing_buttons')) {
                            				        sti_sharing_buttons(true, array('url' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['url'], 'image' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['image'], 'title' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['title'], 'description' => $scroll_content[$one_scroll_content['key']][$one_scroll_content['index']]['content'], "buttons" => $socialmediabtns));
                                				}
                                			?>
                            			</div>
                            			<?php
					               }
					           }
					           ?>
                        </div>
                        <div class="d-flex flex-column" style="width: 10%; margin-left: auto; margin-right: 0;">
                        <?php
                            $socialmedia = get_social_media_array(true);
                            foreach ($socialmedia as $onesocialmedia) {
                                if ($onesocialmedia['tech_name'] == 'link') {
                                    continue;
                                }
                                if ($onesocialmedia['value']) {
                                    ?>
                                    <div class='row'>
                                    	<i id="target_<?php echo $onesocialmedia['tech_name'];?>" <?php echo "style='min-width: 100%; max-width: 100%; padding-right: 0; margin-bottom: 20px; " . (($onesocialmedia['color'] != null)?("color:" . $onesocialmedia['color']):"") . "';"; ?> class='target text-center text-nowrap <?php echo (($onesocialmedia['fa'] == null)?("fa fa-" . $onesocialmedia['tech_name']):$onesocialmedia['fa']); ?> fa-lg'></i>
                                	</div>
                                	<?php
                                }
                            }
                            ?>
                                <div class='row'>
                                	<i id="trash" style='min-width: 100%; max-width: 100%; padding-right: 0; margin-bottom: 20px;' class='target text-center text-nowrap fa fa-trash fa-lg'></i>
                            	</div>
                            	<?php
                            ?>
                        </div>
                	</div>
    			</div>
            </div>
        <?php 
    }
    add_shortcode('posting_scrolls', 'posting_scrolls_function');
}
?>