<?php 
require_once('../../../../../wp-load.php');
$agentid = ire_synch_user_and_agent(true);
if ($agentid != false) {
    if (isset($_GET['propertieslist'])) {
        // Step 1: we remove all properties previously associated with the agent
        $args = array(
            'post_type' => 'property',
            'meta_query' => array(
                array(
                    'key' => 'cobroker_agentid',
                    'value' => $agentid,
                    'compare' => '=',
                )
            )
        );
        $query = new WP_Query($args);
        while ($query->have_posts()) {
            $query->the_post();
            delete_post_meta(get_the_ID(), 'cobroker_agentid'/*, $agentid*/);
        }
        wp_reset_postdata();
        
        $usestr = urldecode($_GET['propertieslist']);
        $usestr = str_replace("\"", "", $usestr);
        $usestr = str_replace("[", "", $usestr);
        $usestr = str_replace("]", "", $usestr);
        $usestr = str_replace("\\", "", $usestr);
                        
        // Step 2: we add all properties now associated with the agent
        $args = array(
            'post_type' => 'property',
            'post__in' => explode(',', $usestr)
        );
        $query = new WP_Query($args);
        while ($query->have_posts()) {
            $query->the_post();
            add_post_meta(get_the_ID(), 'cobroker_agentid', $agentid, false);
        }
        wp_reset_postdata();
    }
}
echo $agentid;

// print_r(get_post_custom([$agentid][0]));

?>