<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/page-templates/home.php'); ?>

<?php
/*
 * Template Name: Home
 */
get_header();
?>



<div id="newContent">

	<main id="newMain" style="padding-top: 70px;">
			
					
                <?php
                // get_template_part( 'partials/home/search' );
                // get_template_part( 'partials/home/how-it-works' );
                ?>
				
				<?php
    get_template_part('partials/header/map');
    ?>
				  
				  <div style="padding-top: 15px;"></div>
				  
				  <?php
    $count_posts = wp_count_posts('property');
    $published_posts = $count_posts->publish;
    get_template_part('partials/home/counter');
    if ($published_posts > 0) {
        get_template_part('partials/home/search', 'twenty');
        get_template_part('partials/home/properties', 'twenty');
        ?> 
                      		<script>
								var loadedCountry = null;
								var loadedLifeStyle = null;
                      		
                          		function setCookie(name, value, days) {
                          			console.log("setting cookie '" + name + "' at " + value);
                          	        var expires = "";
                          	        if (days) {
                          	            var date = new Date();
                          	            date.setTime(date.getTime() + (days*24*60*60*1000));
                          	            expires = "; expires=" + date.toUTCString();
                          	        }
                          	        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
                          	    }
                          	    
                          		function getCookie(name) {
                          	        var nameEQ = name + "=";
                          	        var ca = document.cookie.split(';');
                          	        for (var i=0; i < ca.length; i++) {
                          	             var c = ca[i];
                          	             while (c.charAt(0)==' ') c = c.substring(1, c.length);
                          	                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                          	        }
                          	        return null;
                          	    }

                          		function eraseCookie(name) {   
                          	        document.cookie = name+'=; Max-Age=-99999999;';  
                          	    }

                          		function updatePropertiesForCountry(event) {
                              		if (((event.target == jQuery("#country_listing1")[0]) || (event.target == jQuery("#countryname")[0])) && (event.target.value !== getCookie('country'))) {
                              			if (event.target.value !== '') {
                                  			eraseCookie('country');
                                            setCookie('country', event.target.value, 7);
                              			}
                              			var targetValue = event.target.value;
                              			try {
                                  			if ((event.target == jQuery("#countryname")[0]) && (jQuery("#country_listing1 option[value='" + targetValue + "']").length > 0)) jQuery("#country_listing1").val(event.target.value).change();
                              			} catch(err) {}
                              			try {
                              				if ((event.target == jQuery("#country_listing1")[0]) && (jQuery("#countryname option[value='" + targetValue + "']").length > 0)) jQuery("#countryname").val(event.target.value).change();
                              			} catch(err) {}
                                        updateProperties();
                              		}
                          		}

                          		function updatePropertiesForLifeStyle(event) {
                              		if (event.target == jQuery('#lifestyles1')[0]) {
                              			if (jQuery('#s') != undefined) jQuery('#s').val('');
                          	            eraseCookie('lifestyle');
                              	        setCookie('lifestyle', jQuery('#lifestyles1').val(), 7);
                                        updateProperties();
                              		}
                          		}
                          	    
                          	    function updateProperties() {
                              	    console.log("updateProperties");
                          		    var countryCookie = getCookie('country');
                          	        var lifeStyleCookie = getCookie('lifestyle');
                          		    if ((lifeStyleCookie == null) || (lifeStyleCookie == undefined) || (lifeStyleCookie === '')) {
                        		    	eraseCookie('lifestyle');
                        		    	lifeStyleCookie = '';
                        		    	setCookie('lifestyle', lifeStyleCookie, 7);
                        		    }
                          		    if ((loadedCountry === countryCookie) && (loadedLifeStyle === lifeStyleCookie)) {
                              		    return;
                          		    }
                          		    jQuery(document).off('change', '#country_listing1', updatePropertiesForCountry);
                      		      	jQuery(document).off('change', '#countryname', updatePropertiesForCountry);
                      		      	jQuery(document).off('change', '#lifestyles1', updatePropertiesForLifeStyle);
                      		                             		         
                      		        if ((typeof map !== "undefined") && (countryCookie != undefined)) {
                      		        	centerOnJurisdiction(map, countryCookie);
                      		        }
                  		            var template = "<div class = 'col-md-12' style='text-align: center;'> \
                  		                <a name='properties_list'> \
                  		                    <div id='ajax-load-more' class='ajax-load-more-wrap blue alm-0 alm-loading' data-alm-id='0' data-canonical-url='/news/' data-slug='home' data-post-id='0' data-localized='ajax_load_more_vars'> \
                  		                        <div class='alm-btn-wrap' style='visibility: visible;'> \
                  		                            <button class='alm-load-more-btn more loading' rel='next'>Searching Properties</button> \
                  		                        </div> \
                  		                        <div class='alm-no-results' style='display: none;'>No Property Found</div> \
                  		                    </div> \
                  		                </a> </div>";
                  		            $("#aftersearchshow").html(template);
                  		            
                  		            console.log("Loading more (1690)...");
                  		            
                  		            jQuery.ajax({
                  		                method:'POST',
                  		                url: '/wp-admin/admin-ajax.php',
                  		                data: {action: 'folder_contents', country:countryCookie, keyword:lifeStyleCookie},
                  		                success: function(res) {
                  		                  jQuery('#countryheadingshow').html(countryCookie); 
                  		                  $("#aftersearchshow").html(res);
                  		                }
                  		            });
                  		          	loadedCountry = countryCookie;
                  		          	loadedLifeStyle = lifeStyleCookie;
                      		      	jQuery(document).on('change', '#country_listing1', updatePropertiesForCountry);
                      		      	jQuery(document).on('change', '#countryname', updatePropertiesForCountry);
                    		      	jQuery(document).on('change', '#lifestyles1', updatePropertiesForLifeStyle);
                      		    }
                      		    if ((getCookie('country') == null) || (getCookie('country') == undefined) || (getCookie('country') === '')) {
                      		    	eraseCookie('country');
                      		    	setCookie('country', 'Worldwide', 7);
                      		    }
                      		    <?php 
                      		    if (isset($_GET['keyword'])) { ?>
                      		  		eraseCookie("lifestyle");
              		    			setCookie("lifestyle", "<?php echo $_GET['keyword'];?>", 7);
                      		  		jQuery(document).ready(function() {
                          		    	eraseCookie("lifestyle");
                        		    	setCookie("lifestyle", "<?php echo $_GET['keyword'];?>", 7);
                        		    	if (jQuery("#s") != undefined) {
                        		    		jQuery("#s").val("<?php echo $_GET['keyword'];?>");
                        		    		var timer = '';
                        		    		jQuery("#s").keyup(function(e) {
                            		    		clearTimeout(timer);
                            		    		timer = setTimeout(function() {
                            		    			setCookie("lifestyle", jQuery("#s").val(), 7);
                                		    		if (jQuery("#s").val() == '') {
                                		    			jQuery("#lifestyles1").val('');
                                		    			updateProperties();
                                		    		}
                            		    		}, 500);
                            		    	});
                        		    	}
                      		  		});
                    		    <?php 
                      		    } else { ?>
                      		    	var originalCookie = getCookie('lifestyle');
                          		    if ((originalCookie == null) || (originalCookie == undefined) || (originalCookie === '') || ((originalCookie != undefined) && (originalCookie != null) && (originalCookie != '') && (jQuery("#lifestyles1 option[value='" + originalCookie + "']").length == 0))) {
                        		    	eraseCookie('lifestyle');
                        		    	setCookie('lifestyle', '', 7);
                        		    }
                      		    <?php 
                                } ?>
                                var targetValue = getCookie("country");
                  		      	jQuery('#countryname').find("option[value='" + targetValue + "']").attr("selected",true);
                  		      	jQuery('#country_listing1').find("option[value='" + targetValue + "']").attr("selected",true);
                  		      	targetValue = getCookie("lifestyle");
                  		      	jQuery('#lifestyles1').find("option[value='" + targetValue + "']").attr("selected",true);
                  		      	if ((jQuery('#lifestyles1').val() == undefined) || (jQuery('#lifestyles1').val() == '')) {
                      		      	jQuery('#lifestyles1').find('option[value=""]').attr("selected",true);
                  		      	}
                          	  	updateProperties();
                  		      	jQuery(document).on('change', '#country_listing1', updatePropertiesForCountry);
                  		      	jQuery(document).on('change', '#countryname', updatePropertiesForCountry);
                  		      	jQuery(document).on('change', '#lifestyles1', updatePropertiesForLifeStyle);
                      	</script> 
						
                      <?php
    } else {
        ?>
                      <div class="alert alert-danger" role="alert"
			style="margin: 30px">
			<h4 class="alert-heading">No properties!</h4>
			<p>EveryListing.com seems to have a personality defect and now be
				NoListing.com! Fill it with properties please...</p>
		</div>
                      <?php
    }
    ?>
            </main>
	<!-- .site-main -->

</div>
<!-- .site-content -->

<script>
$( document ).ready(function() {
	var urlParams = new URLSearchParams(location.search);
	var myParam = decodeURIComponent(urlParams.get('login'));
	console.log("******* URL parameters:	" + myParam );   
	//document.getElementById('login-username').value = myParam;
	if( myParam != 'null' && myParam != 'undefined'){
		$('#loginhyperlink').trigger('click');
		document.getElementById('login-username').value = myParam;
	}
	
});
</script>

<?php
get_footer();
?>
