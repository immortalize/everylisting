<?php
/**
* Plugin Name: Chat Session Addon
* Plugin URI: https://www.quantumcloud.com/products/plugins/
* Description: WPBot Chat History addon for saving WPBot chat history. 
* Version: 2.1.0
* Author: QunatumCloud
* Author URI: https://www.quantumcloud.com/
* Requires at least: 4.6
* Tested up to: 5.3
* License: GPL2
*/


if(!function_exists('qcwp_chat_session_menu_fnc')){
defined('ABSPATH') or die("No direct script access!");
define('QCLD_wpCHATBOT_HISTORY_PLUGIN_URL', plugin_dir_url(__FILE__));
require('plugin-upgrader/plugin-upgrader.php');
add_action('init', 'qcpd_wpcs_chat_session_dependencies');
function qcpd_wpcs_chat_session_dependencies(){
	include_once(ABSPATH.'wp-admin/includes/plugin.php');
	if ( !class_exists('qcld_wb_Chatbot') && !class_exists('QCLD_Woo_Chatbot') && (qcwpcs_is_kbxwpbot_active() != true) ) {
		add_action('admin_notices', 'qcpd_wpbot_cs_require_notice');
	} 
}

add_action( 'admin_menu', 'qcwp_chat_session_menu_fnc' );

function qcwp_chat_session_menu_fnc(){
	
	if ( current_user_can( 'publish_posts' ) ){


		add_menu_page( 'Bot - Sessions', 'Bot - Sessions', 'publish_posts', 'wbcs-botsessions-page', 'qc_wpbot_cs_menu_page_callback_func', 'dashicons-menu', '9' );
		
		if(qcpdcs_is_woowbot_active()){
			add_submenu_page( 'wbcs-botsessions-page', 'ChatBot Sessions', 'ChatBot Sessions', 'manage_options','woowbot_cs_menu_page', 'woowbot_cs_menu_page_callback_func' );
		}
		
		add_submenu_page( 'wbcs-botsessions-page', 'Help & License', 'Help & License', 'manage_options','chatbot-sessions-help-license', 'qcld_chatbot_sessions_license_callback' );


	}
	
}

function qcpd_wpbot_cs_require_notice()
{
?>
	<div id="message" class="error">
		<p>
			<?php echo esc_html('Please install & activate the WPBot pro or WoowBot WooCommerce Chatbot Pro plugin to get the Chat Session Addon work properly.') ?>
		</p>
	</div>
<?php
}

function qc_wpbot_cs_menu_page_callback_func(){
	global $wpdb;
	$wpdb->show_errors = true;
	
	$tableuser    = $wpdb->prefix.'wpbot_user';
	$tableconversation    = $wpdb->prefix.'wpbot_Conversation';
	$msg = '';
	if(isset($_GET['action']) && $_GET['action']=='deleteall'){
		global $wpdb;
		$wpdb->query("TRUNCATE TABLE `$tableuser`");
		$wpdb->query("TRUNCATE TABLE `$tableconversation`");
		$msg = esc_html('All Sessions has been deleted successfully!');
	}
	
	if(isset($_GET['msg']) && $_GET['msg']=='success'){
		echo '<div class="notice notice-success"><p>Record has beed Deleted Successfully!</p></div>';
	}
	
	if(isset($_GET['userid']) && $_GET['userid']!=''){
	$userid = sanitize_text_field($_GET['userid']);
	$userinfo = $wpdb->get_row("select * from $tableuser where 1 and id = '".$userid."'");
	$delurl = admin_url( 'admin.php?page=wbcs-botsessions-page&userid='.$userinfo->id.'&act=delete');
	?>	
	<div class="sld_menu_title" style="text-align:left;">
		<table class="form-table">
			<tbody>
			<tr><th style="padding: 5px;" scope="row"><?php echo esc_html('User Name'); ?></th><td style="padding: 5px;"><?php echo esc_html($userinfo->name) ?></td></tr>
			<tr><th style="padding: 5px;" scope="row"><?php echo esc_html('User Email') ?></th><td style="padding: 5px;"><?php echo esc_html($userinfo->email) ?></td></tr>
			<tr><th style="padding: 5px;" scope="row"><?php echo esc_html('Date'); ?></th><td style="padding: 5px;"><?php echo date('M,d,Y h:i:s A', strtotime($userinfo->date)); ?></td></tr>
			</tbody>
		</table>
		
		<a href="<?php echo esc_url($delurl); ?>" class="button-primary" onclick="return confirm('are you sure?')" style="float: right; position: relative; top: -65px; right: 21px;"><?php echo esc_html('Delete'); ?></a>
	</div>
	<?php 
	
	$result = $wpdb->get_row("select * from $tableconversation where 1 and user_id = '".$userid."'");
	
		if(!empty($result)):
		
		$qcld_wb_chatbot_theme = get_option('qcld_wb_chatbot_theme');
		if (file_exists(QCLD_wpCHATBOT_PLUGIN_DIR_PATH . '/templates/' . $qcld_wb_chatbot_theme . '/style.css')) {
			wp_register_style('qcld-wp-chatbot-style', QCLD_wpCHATBOT_PLUGIN_URL . '/templates/' . $qcld_wb_chatbot_theme . '/style.css', '', QCLD_wpCHATBOT_VERSION, 'screen');
			wp_enqueue_style('qcld-wp-chatbot-style');
		}
		wp_register_style('qcld-wp-chatbot-history-style', QCLD_wpCHATBOT_HISTORY_PLUGIN_URL . '/css/history-style.css', '', QCLD_wpCHATBOT_VERSION, 'screen');
        wp_enqueue_style('qcld-wp-chatbot-history-style');
		wp_register_style('qcld-wp-chatbot-common-style', QCLD_wpCHATBOT_PLUGIN_URL . '/css/common-style.css', '', QCLD_wpCHATBOT_VERSION, 'screen');
        wp_enqueue_style('qcld-wp-chatbot-common-style');
		
		
	?>
		<div class="qchero_sliders_list_wrapper">
		<div class="qchero_slider_table_area" style="max-width: 650px;">
			<div class="wp-chatbot-messages-wrapper">
			<?php echo htmlspecialchars_decode($result->conversation); ?>
			</div>
		</div>
		</div>
	<?php
		endif;
	}else{
		wp_register_style('qcld-wp-chatbot-history-style', QCLD_wpCHATBOT_HISTORY_PLUGIN_URL . '/css/history-style.css', '', QCLD_wpCHATBOT_VERSION, 'screen');
        wp_enqueue_style('qcld-wp-chatbot-history-style');
		
		wp_register_script('qcld-wp-chatsession-admin-js',QCLD_wpCHATBOT_HISTORY_PLUGIN_URL . '/js/chatsession.js' , array('jquery'), true);
         wp_enqueue_script('qcld-wp-chatsession-admin-js');
		 wp_localize_script('qcld-wp-chatsession-admin-js', 'ajax_object',
                array('ajax_url' => admin_url('admin-ajax.php')));
		
		$sql = "select * from $tableuser where 1 order by `date` DESC";
		$result1 = $wpdb->get_results($sql);
		$sql1 = "SELECT count(*) FROM $tableuser where 1";
		
		$total             = $wpdb->get_var( $sql1 );
		$items_per_page = 30;
		$page             = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
		$offset         = ( $page * $items_per_page ) - $items_per_page;
		$sql .=" LIMIT ${offset}, ${items_per_page}";
		$rows = $wpdb->get_results( $sql );
		$totalPage         = ceil($total / $items_per_page);
		$result = $wpdb->get_results($sql);
		$customPagHTML = '';
		if($totalPage > 1){
			$customPagHTML     =  '<div><span class="wpbot_pagination">Page '.esc_html($page).' of '.esc_html($totalPage).'</span>'.paginate_links( array(
			'base' => add_query_arg( 'cpage', '%#%' ),
			'format' => '',
			'prev_text'    => __('« prev'),
            'next_text'    => __('next »'),
			'total' => esc_html($totalPage),
			'current' => esc_html($page),
			
			)).'</div>';
		}
$deleteurl = admin_url( 'admin.php?page=wbcs-botsessions-page&action=deleteall');
	?>
	<div class="qchero_sliders_list_wrapper">
		<?php 
			if($msg!=''){
				?>
				<div class="notice notice-success is-dismissible">
					<p><?php echo esc_html($msg); ?></p>
				</div>
				<?php
			}
		?>
		<div class="sld_menu_title">
			<h2 style="font-size: 26px;text-align:center"><?php echo esc_html__('Chat Sessions', 'qc-opd').' ('.count($result1).')'; ?></h2>
		</div>
		
		<?php if($customPagHTML!=''): ?>
		<div class="sld_menu_title sld_menu_title_align"><?php echo ($customPagHTML); ?> </div>
		<?php endif; ?>
		
		
		
		<form id="wpcs_form_sessions" action="<?php echo esc_url($mainurl); ?>" method="POST" style="width:100%">
		<input type="hidden" name="wpbot_session_remove" />
		<br>
		<button class="button-primary" id="wpbot_submit_delcs_form"><?php echo esc_html('Delete'); ?></button>
		<a href="<?php echo esc_url($deleteurl); ?>" class="button button-primary" ><?php echo esc_html('Delete All Sessions'); ?></a>
		
		<?php if(!empty($result)): ?>
		
		<div class="qchero_slider_table_area">
			<div class="sld_payment_table">
				<div class="sld_payment_row header">
				
					<div class="sld_payment_cell">
						<input type="checkbox" id="wpbot_checked_all" />
					</div>
					
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Date', 'qc-opd' ) ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'User Interaction Count', 'qc-opd' ) ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Session ID', 'qc-opd' ) ?>
					</div>
					
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Name', 'qc-opd' ); ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Email', 'qc-opd' ); ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Phone', 'qc-opd' ); ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Action', 'qc-opd' ); ?>
					</div>
					
				</div>

		<?php
		foreach($result as $row){
			$url = admin_url( 'admin.php?page=wbcs-botsessions-page&userid='.$row->id);
			$delurl = admin_url( 'admin.php?page=wbcs-botsessions-page&userid='.$row->id.'&act=delete');
		?>
			<div class="sld_payment_row">
				
				<div class="sld_payment_cell">
					
					<input type="checkbox" name="sessions[]" class="wpbot_sessions_checkbox" value="<?php echo esc_html($row->id) ?>" />
				</div>
				
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Date', 'qc-opd') ?></div>
					<a href="<?php echo esc_url($url); ?>"><?php echo date('M,d,Y h:i:s A', strtotime($row->date)); ?></a>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('User Interaction Count', 'qc-opd') ?></div>
					<?php
						$res = $wpdb->get_row("select * from $tableconversation where 1 and user_id = '".$row->id."'");
						echo substr_count($res->conversation, "wp-chat-user-msg");
					?>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Session ID', 'qc-opd') ?></div>
					<?php echo esc_html($row->session_id); ?>
				</div>
				
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Name', 'qc-opd') ?></div>
					<?php echo esc_html($row->name); ?>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Email', 'qc-opd') ?></div>
					<?php
						echo esc_html($row->email);
					?>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Phone', 'qc-opd') ?></div>
					<?php
						echo esc_html($row->phone);
					?>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Action', 'qc-opd') ?></div>
					<a href="<?php echo esc_url($url); ?>" class="button-primary"><?php echo esc_html('View Chat') ?></a>
					<a href="<?php echo esc_url($delurl); ?>" class="button-primary" onclick="return confirm('are you sure?')"><?php echo esc_html('Delete'); ?></a>
					<?php if($row->email!=''): ?>
					<a href="#" data-email="<?php echo esc_html($row->email); ?>" class="button-primary wpcsmyBtn"><?php echo esc_html('Send Email') ?></a>
					<?php endif; ?>
				</div>
				
			</div>
		<?php
		}
		?>

		</div>

	</div>
	</form>
	<?php endif; ?>
	</div>
	<?php
	}
}

function woowbot_cs_menu_page_callback_func(){
	global $wpdb;
	$wpdb->show_errors = true;
	
	$tableuser    = $wpdb->prefix.'wowbot_user';
	$tableconversation    = $wpdb->prefix.'wowbot_Conversation';

	
	$msg = '';
	if(isset($_GET['action']) && $_GET['action']=='deleteall'){
		global $wpdb;
		$wpdb->query("TRUNCATE TABLE `$tableuser`");
		$wpdb->query("TRUNCATE TABLE `$tableconversation`");
		$msg = esc_html('All Sessions has been deleted successfully!');
	}
	
	if(isset($_GET['msg']) && $_GET['msg']=='success'){
		echo '<div class="notice notice-success"><p>Record has beed Deleted Successfully!</p></div>';
	}
	
	if(isset($_GET['userid']) && $_GET['userid']!=''){
	$userid = $_GET['userid'];
	$userinfo = $wpdb->get_row("select * from $tableuser where 1 and id = '".$userid."'");
	?>	
	<div class="sld_menu_title" style="text-align:left;">
		<table class="form-table">
			<tbody>
			<tr><th style="padding: 5px;" scope="row"><?php echo esc_html('User Name'); ?></th><td style="padding: 5px;"><?php echo esc_html($userinfo->name); ?></td></tr>
			<tr><th style="padding: 5px;" scope="row"><?php echo esc_html('User Email'); ?></th><td style="padding: 5px;"><?php echo esc_html($userinfo->email) ?></td></tr>
			<tr><th style="padding: 5px;" scope="row"><?php echo esc_html('Date'); ?></th><td style="padding: 5px;"><?php echo date('M,d,Y h:i:s A', strtotime($userinfo->date)); ?></td></tr>
			</tbody>
		</table>
	</div>
	<?php 
	
	$result = $wpdb->get_row("select * from $tableconversation where 1 and user_id = '".$userid."'");
	
		if(!empty($result)):
		
		$qcld_wb_chatbot_theme = get_option('qcld_woo_chatbot_theme');
		if (file_exists(QCLD_WOOCHATBOT_PLUGIN_DIR_PATH . '/templates/' . $qcld_wb_chatbot_theme . '/style.css')) {
			wp_register_style('qcld-wp-chatbot-style', QCLD_WOOCHATBOT_PLUGIN_URL . '/templates/' . $qcld_wb_chatbot_theme . '/style.css', '', QCLD_WOOCHATBOT_VERSION, 'screen');
			wp_enqueue_style('qcld-wp-chatbot-style');
		}
		wp_register_style('qcld-wp-chatbot-history-style', QCLD_wpCHATBOT_HISTORY_PLUGIN_URL . '/css/history-style.css', '', QCLD_WOOCHATBOT_VERSION, 'screen');
        wp_enqueue_style('qcld-wp-chatbot-history-style');
		wp_register_style('qcld-wp-chatbot-common-style', QCLD_WOOCHATBOT_PLUGIN_URL . '/css/common-style.css', '', QCLD_WOOCHATBOT_VERSION, 'screen');
        wp_enqueue_style('qcld-wp-chatbot-common-style');
		
		
	?>
		<div class="qchero_sliders_list_wrapper">
		<div class="qchero_slider_table_area" style="max-width: 650px;">
			<div id="woo-chatbot-shortcode-template-container" class="wp-chatbot-messages-wrapper">
			<?php echo htmlspecialchars_decode($result->conversation); ?>
			</div>
		</div>
		</div>
	<?php
		endif;
	}else{
		wp_register_style('qcld-wp-chatbot-history-style', QCLD_wpCHATBOT_HISTORY_PLUGIN_URL . '/css/history-style.css', '', QCLD_wpCHATBOT_VERSION, 'screen');
        wp_enqueue_style('qcld-wp-chatbot-history-style');
		
		 wp_register_script('qcld-wp-chatsession-admin-js',QCLD_wpCHATBOT_HISTORY_PLUGIN_URL . '/js/chatsession.js' , array('jquery'), true);
         wp_enqueue_script('qcld-wp-chatsession-admin-js');
		wp_localize_script('qcld-wp-chatsession-admin-js', 'ajax_object',
                array('ajax_url' => admin_url('admin-ajax.php')));
		
		
		$sql = "select * from $tableuser where 1 order by `date` DESC";
		$result1 = $wpdb->get_results($sql);
		$sql1 = "SELECT count(*) FROM $tableuser where 1";
		
		$total             = $wpdb->get_var( $sql1 );
		$items_per_page = 30;
		$page             = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
		$offset         = ( $page * $items_per_page ) - $items_per_page;
		$sql .=" LIMIT ${offset}, ${items_per_page}";
		$rows = $wpdb->get_results( $sql );
		$totalPage         = ceil($total / $items_per_page);
		$result = $wpdb->get_results($sql);

		if($totalPage > 1){
			$customPagHTML     =  '<div><span class="wpbot_pagination">Page '.esc_html($page).' of '.esc_html($totalPage).'</span>'.paginate_links( array(
			'base' => add_query_arg( 'cpage', '%#%' ),
			'format' => '',
			'prev_text'    => __('« prev'),
            'next_text'    => __('next »'),
			'total' => esc_html($totalPage),
			'current' => esc_html($page),
			
			)).'</div>';
		}
		$mainurl = admin_url( 'admin.php?page=woowbot_cs_menu_page');
		$deleteurl = admin_url( 'admin.php?page=woowbot_cs_menu_page&action=deleteall');
	?>
	<div class="qchero_sliders_list_wrapper">
	
		<?php 
			if($msg!=''){
				?>
				<div class="notice notice-success is-dismissible">
					<p><?php echo esc_html($msg); ?></p>
				</div>
				<?php
			}
		?>
	
		<div class="sld_menu_title">
			<h2 style="font-size: 26px;text-align:center"><?php echo esc_html__('Chat Sessions', 'qc-opd').' ('.count($result1).')'; ?></h2>
		</div>
		<?php if($customPagHTML!=''): ?>
		<div class="sld_menu_title sld_menu_title_align"><?php echo ($customPagHTML); ?> </div>
		<?php endif; ?>
		<?php if(!empty($result)): ?>
		
		<form id="wpcs_form_sessions" action="<?php echo esc_url($mainurl); ?>" method="POST" style="width:100%">
		<input type="hidden" name="wowbot_session_remove" />
		<br>
		<button class="button-primary" id="wpbot_submit_delcs_form"><?php echo esc_html('Delete'); ?></button>
		<a href="<?php echo esc_url($deleteurl); ?>" class="button button-primary" ><?php echo esc_html('Delete All Sessions'); ?></a>
		<div class="qchero_slider_table_area">
			<div class="sld_payment_table">
				<div class="sld_payment_row header">
					
					<div class="sld_payment_cell">
						<input type="checkbox" id="wpbot_checked_all" />
					</div>
					
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Date', 'qc-opd' ) ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'User Interaction Count', 'qc-opd' ) ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Session ID', 'qc-opd' ) ?>
					</div>
					
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Name', 'qc-opd' ); ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Email', 'qc-opd' ); ?>
					</div>
					<div class="sld_payment_cell">
						<?php echo esc_html__( 'Action', 'qc-opd' ); ?>
					</div>
					
				</div>

		<?php
		foreach($result as $row){
			$url = admin_url( 'admin.php?page=woowbot_cs_menu_page&userid='.$row->id);
			$delurl = admin_url( 'admin.php?page=woowbot_cs_menu_page&userid='.$row->id.'&act=delete');
		?>
			<div class="sld_payment_row">
			
				<div class="sld_payment_cell">
					
					<input type="checkbox" name="sessions[]" class="wpbot_sessions_checkbox" value="<?php echo esc_html($row->id) ?>" />
				</div>
				
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Date', 'qc-opd') ?></div>
					<a href="<?php echo esc_url($url); ?>"><?php echo date('M,d,Y h:i:s A', strtotime($row->date)); ?></a>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('User Interaction Count', 'qc-opd') ?></div>
					<?php
						$res = $wpdb->get_row("select * from $tableconversation where 1 and user_id = '".$row->id."'");
						echo substr_count($res->conversation, "woo-chat-user-msg");
					?>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Session ID', 'qc-opd') ?></div>
					<?php echo esc_html($row->session_id); ?>
				</div>
				
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Name', 'qc-opd') ?></div>
					<?php echo esc_html($row->name); ?>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Email', 'qc-opd') ?></div>
					<?php
						echo esc_html($row->email);
					?>
				</div>
				<div class="sld_payment_cell">
					<div class="sld_responsive_head"><?php echo esc_html__('Action', 'qc-opd') ?></div>
					<a href="<?php echo esc_url($url); ?>" class="button-primary"><?php echo esc_html('View Chat'); ?></a>
					<a href="<?php echo esc_url($delurl); ?>" class="button-primary" onclick="return confirm('are you sure?')"><?php echo esc_html('Delete'); ?></a>
					<?php if($row->email!=''): ?>
					<a href="#" data-email="<?php echo esc_html($row->email); ?>" class="button-primary wpcsmyBtn"><?php echo esc_html('Send Email'); ?></a>
					<?php endif; ?>
				</div>
				
			</div>
		<?php
		}
		?>

		</div>

	</div>
	</form>
	<?php endif; ?>
	</div>
	<?php
	}
}

if(!function_exists('qcwp_isset_table_column')) {
	function qcwp_isset_table_column($table_name, $column_name)
	{
		global $wpdb;
		$columns = $wpdb->get_results("SHOW COLUMNS FROM  " . $table_name, ARRAY_A);
		foreach ($columns as $column) {
			if ($column['Field'] == $column_name) {
				return true;
			}
		}
	}
}

register_activation_hook(__FILE__, 'qcld_wb_chatboot_sessions_defualt_options');
function qcld_wb_chatboot_sessions_defualt_options(){
	global $wpdb;
	$collate = '';

	if ( $wpdb->has_cap( 'collation' ) ) {

		if ( ! empty( $wpdb->charset ) ) {

			$collate .= "DEFAULT CHARACTER SET $wpdb->charset";
		}
		if ( ! empty( $wpdb->collate ) ) {

			$collate .= " COLLATE $wpdb->collate";

		}
	}
	


    //Bot User Table
    $table1    = $wpdb->prefix.'wpbot_user';
	$sql_sliders_Table1 = "
		CREATE TABLE IF NOT EXISTS `$table1` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `session_id` varchar(256) NOT NULL,
          `name` varchar(256) NOT NULL,
          `email` varchar(256) NOT NULL,
		  `date` datetime NOT NULL,
		  PRIMARY KEY (`id`)
		)  $collate AUTO_INCREMENT=1 ";
		
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql_sliders_Table1 );
	
	if ( ! qcwp_isset_table_column( $table1, 'phone' ) ) {
		$sql_wp_Table_update_1 = "ALTER TABLE `$table1` ADD `phone` varchar(256) NOT NULL;";
		$wpdb->query( $sql_wp_Table_update_1 );
	}

    //Bot User Table
    $table2    = $wpdb->prefix.'wpbot_Conversation';
	$sql_sliders_Table2 = "
		CREATE TABLE IF NOT EXISTS `$table2` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `user_id` int(11) NOT NULL,
          `conversation` LONGTEXT NOT NULL,
		  PRIMARY KEY (`id`)
		)  $collate AUTO_INCREMENT=1 ";
		
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql_sliders_Table2 );
}


function qcld_chatbot_sessions_license_callback(){
	wp_enqueue_script('qcld-wp-chatsession-admin-lisence-js',QCLD_wpCHATBOT_HISTORY_PLUGIN_URL . '/js/admin.js' , array('jquery'), true);
    
	?>
	<div id="licensing">
				<h1><?php echo esc_html('Please Insert your license Key'); ?></h1>
				<?php if( get_chatsessions_valid_license() ){ ?>
					<div class="qcld-success-notice">
						<p><?php echo esc_html('Thank you, Your License is active'); ?></p>
					</div>
				<?php } ?>
				
				<?php
				
					$track_domain_request = wp_remote_get(chatsessions_LICENSING_PRODUCT_DEV_URL."wp-json/qc-domain-tracker/v1/getdomain/?license_key=".get_chatsessions_licensing_key());
					if( !is_wp_error( $track_domain_request ) || wp_remote_retrieve_response_code( $track_domain_request ) === 200 ){
						$track_domain_result = json_decode($track_domain_request['body']);
						
						$max_domain_num = $track_domain_result[0]->max_domain + 1;
						$total_domains = @json_decode($track_domain_result[0]->domain, true);
						if(!empty($total_domains)){
						$total_domains_num = count($total_domains);

						if( $max_domain_num <= $total_domains_num){
					?>
							<div class="qcld-error-notice">
								<p><?php echo esc_html('You have activated this key for maximum number of sites allowed by your license. Please'); ?> <a href='<?php echo esc_url('https://www.quantumcloud.com/products/'); ?>'><?php echo esc_html('purchase additional license'); ?></a></p>
							</div>
					<?php
						}
						}
						
					}
				?>
				
				<form onsubmit="return false" id="qc-license-form" method="post" action="options.php">
					<?php
						delete_chatsessions_update_transient();
						delete_chatsessions_renew_transient();
						
						delete_option('_site_transient_update_plugins');
						settings_fields( 'qcld_chatsessions_license' );
						do_settings_sections( 'qcld_chatsessions_license' );

						// if( isset($_POST['submit']) ){
						// 	echo 'qcld_chatsessions_buy_from_where '.$_POST['qcld_chatsessions_buy_from_where'];
						// }
					?>
					<table class="form-table">
						

						<tr id="quantumcloud_portfolio_license_row" style="display: none">
							<th>
								<label for="qcld_chatsessions_enter_license_key"><?php echo esc_html('Enter License Key:'); ?></label>
							</th>
							<td>
								<input type="<?php echo (get_chatsessions_licensing_key()!=''?'password':'text'); ?>" id="qcld_chatsessions_enter_license_key" name="qcld_chatsessions_enter_license_key" class="regular-text" value="<?php echo get_chatsessions_licensing_key(); ?>">
								<p><?php echo esc_html('You can copy the license key from'); ?> <a target="_blank" href='<?php echo esc_url('https://www.quantumcloud.com/products/account/'); ?>'><?php echo esc_html('your account'); ?></a></p>
							</td>
						</tr>

						<tr id="show_envato_plugin_downloader" style="display: none">
							<th>
								<label for="qcld_chatsessions_enter_envato_key"><?php echo esc_html('Enter Purchase Code:'); ?></label>
							</th>
							<td colspan="4">
								<input type="<?php echo (get_chatsessions_envato_key()!=''?'password':'text'); ?>" id="qcld_chatsessions_enter_envato_key" name="qcld_chatsessions_enter_envato_key" class="regular-text" value="<?php echo get_chatsessions_envato_key(); ?>">
								<p><?php echo esc_html('You can install the'); ?> <a target="_blank" href="https://envato.com/market-plugin/"><?php echo esc_html('Envato Plugin'); ?></a> <?php echo esc_html('to stay up to date.'); ?></p>
							</td>
						</tr>
						
						<tr>
							<th>
								<label for="qcld_chatsessions_enter_license_or_purchase_key"><?php echo esc_html('Enter License Key or Purchase Code:'); ?></label>
							</th>
							<td>
								<input type="<?php echo (get_chatsessions_license_purchase_code()!=''?'password':'text'); ?>" id="qcld_chatsessions_enter_license_or_purchase_key" name="qcld_chatsessions_enter_license_or_purchase_key" class="regular-text" value="<?php echo get_chatsessions_license_purchase_code(); ?>" required>
							</td>
						</tr>

					</table>
					<!-- //start new-update-for-codecanyon -->
					<input type="hidden" name="qcld_chatsessions_buy_from_where" value="<?php echo get_chatsessions_licensing_buy_from(); ?>" >
					<!-- //end new-update-for-codecanyon -->
					<?php submit_button(); ?>
				</form>
				
			</div>
	<?php
}

//plugin activate redirect codecanyon

function qc_chatsessions_activation_redirect( $plugin ) {
    if( $plugin == plugin_basename( __FILE__ ) ) {
        exit( wp_redirect( admin_url('admin.php?page=chatbot-sessions-help-license') ) );
    }
}
add_action( 'activated_plugin', 'qc_chatsessions_activation_redirect' );

function qcpdcs_is_woowbot_active(){
	if(class_exists('QCLD_Woo_Chatbot')){
        return true;
    }else{
        return false;
    }
}

function qcpdcs_is_wpbot_active(){
	
	if(class_exists('qcld_wb_Chatbot')){
        return true;
    }else{
        return false;
    }
	
}

add_action('init', 'qc_wp_cs_request_handle');

function qc_wp_cs_request_handle(){
	global $wpdb;
	$wpdb->show_errors = true;
	
	$tableuser    = $wpdb->prefix.'wowbot_user';
	$tableconversation    = $wpdb->prefix.'wowbot_Conversation';
	
	$tableuser1    = $wpdb->prefix.'wpbot_user';
	$tableconversation1    = $wpdb->prefix.'wpbot_Conversation';
	
	if(isset($_GET['page']) && $_GET['page']=='woowbot_cs_menu_page' && isset($_GET['act']) && $_GET['act']=='delete'){
		$userid = $_GET['userid'];
		$wpdb->delete(
            "$tableuser",
            array( 'id' => $userid ),
            array( '%d' )
        );
		$wpdb->delete(
            "$tableconversation",
            array( 'user_id' => $userid ),
            array( '%d' )
        );
		wp_redirect(admin_url( 'admin.php?page=woowbot_cs_menu_page&msg=success'));exit;
	}
	
	if(isset($_POST['wowbot_session_remove']) && !empty($_POST['sessions'])){
		
		$userids = $_POST['sessions'];
		foreach($userids as $userid){
			$wpdb->delete(
				"$tableuser",
				array( 'id' => $userid ),
				array( '%d' )
			);
			$wpdb->delete(
				"$tableconversation",
				array( 'user_id' => $userid ),
				array( '%d' )
			);
		}
		wp_redirect(admin_url( 'admin.php?page=woowbot_cs_menu_page&msg=success'));exit;
		
	}
	
	if(isset($_GET['page']) && $_GET['page']=='wbcs-botsessions-page' && isset($_GET['act']) && $_GET['act']=='delete'){
		$userid = $_GET['userid'];
		$wpdb->delete(
            "$tableuser1",
            array( 'id' => $userid ),
            array( '%d' )
        );
		$wpdb->delete(
            "$tableconversation1",
            array( 'user_id' => $userid ),
            array( '%d' )
        );
		wp_redirect(admin_url( 'admin.php?page=wbcs-botsessions-page&msg=success'));exit;
	}
	
	if(isset($_POST['wpbot_session_remove']) && !empty($_POST['sessions'])){
		
		$userids = $_POST['sessions'];
		foreach($userids as $userid){
			$wpdb->delete(
				"$tableuser1",
				array( 'id' => $userid ),
				array( '%d' )
			);
			$wpdb->delete(
				"$tableconversation1",
				array( 'user_id' => $userid ),
				array( '%d' )
			);
		}
		wp_redirect(admin_url( 'admin.php?page=wbcs-botsessions-page&msg=success'));exit;
		
	}
	
}


function qcwpcs_order_menu_submenu(){
	global $submenu;
	
	if(!qcpdcs_is_wpbot_active() && !qcwpcs_is_kbxwpbot_active() ){
		unset($submenu['wbcs-botsessions-page'][0]);
	}
	
	
	
	return $submenu;
}
add_filter( 'custom_menu_order', 'qcwpcs_order_menu_submenu', 1 );

add_action('admin_footer', 'wpcs_admin_footer_content');
function wpcs_admin_footer_content(){
	if((isset($_GET['page']) && $_GET['page']=='wbcs-botsessions-page') || (isset($_GET['page']) && $_GET['page']=='woowbot_cs_menu_page')){
	?>
	<div id="wpcsmyModal" class="wpcsmodal">

	  <!-- Modal content -->
		<div class="wpcsmodal-content">
		<span class="wpcsclose">&times;</span>
		<h2><?php echo esc_html('Send an Email to'); ?> <span id="wpcs_show_email"></span></h2>
		<div class="wpcs_form_container">
		  <form id="wpcs_email_form" action="">
			<label for="fname"><?php echo esc_html('Subject'); ?></label>
			<input type="text" class="wpcs_text_field" id="wpcs_email_subject" name="wpcs_email_subject" placeholder="Subject.." required>
			
			<label for="lname"><?php echo esc_html('Your Message'); ?></label>
			<textarea id="wpcs_email_message" class="wpcs_text_field" name="wpcs_email_message" placeholder="" style="height:200px" required></textarea>
			<input type="hidden" id="wpcs_to_email_address" value="" />
			<input type="submit" class="wpcs_submit_field" id="wpcs_email_submit" value="Submit">
			<span id="wpcs_email_loading" style=" display: none;"><img style="width:20px;" src="<?php echo esc_url(QCLD_wpCHATBOT_HISTORY_PLUGIN_URL.'images/ajax-loader.gif'); ?>"></span>
			<span id="wpcs_email_status"></span>
		  </form>
		</div>
		</div>

	</div>
	<?php
	}
}


function wpcs_send_email() {

		
	$subject = sanitize_text_field($_POST['data']['subject']);
	$message = sanitize_text_field($_POST['data']['message']);
	$to = sanitize_email($_POST['data']['to']);
	
	$url = get_site_url();
    $url = parse_url($url);
    $domain = $url['host'];
	$fromEmail = "wordpress@" . $domain;
	
	$body = $message;

	$headers = array();
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: ' . esc_html($domain) . ' <' . esc_html($fromEmail) . '>';

	$result = wp_mail($to, $subject, $body, $headers);
	if ($result) {
		$response['status'] = 'success';
		$response['message'] = 'Email has been sent successfully!';
	}else{
		$response['status'] = 'fail';
		$response['message'] = 'Unable to send email. Please contact your server administrator.';
	}
    
    ob_clean();
    echo json_encode($response);
    die();

}

add_action( 'wp_ajax_wpcs_send_email',        'wpcs_send_email' );
add_action( 'wp_ajax_nopriv_wpcs_send_email', 'wpcs_send_email' );



function qcwpcs_is_kbxwpbot_active(){

	if ( defined( 'KBX_WP_CHATBOT' ) && (KBX_WP_CHATBOT == '1') ) {
		return true;
	}else{
		return false;
	}
}

if(!function_exists('qcld_wb_chatbot_conversation_save')){
	function qcld_wb_chatbot_conversation_save() {
		
		check_ajax_referer( 'qcsecretbotnonceval123qc', 'security' );
		global $wpdb;

		$tableuser    = $wpdb->prefix.'wpbot_user';
		$tableconversation    = $wpdb->prefix.'wpbot_Conversation';
		
		$conversation = qc_wpbot_input_validation($_POST['conversation']);
		$email = sanitize_email($_POST['email']);
		$phone = sanitize_text_field($_POST['phone']);
		$name = sanitize_text_field($_POST['name']);
		$session_id = sanitize_text_field($_POST['session_id']);

		
		
		$response = array();
		$response['status'] = 'success';
		
		$user_exists = $wpdb->get_row("select * from $tableuser where 1 and session_id = '".$session_id."'");
		if(empty($user_exists)){
		
			$wpdb->insert(
				$tableuser,
				array(
					'date'  => current_time( 'mysql' ),
					'name'   => $name,
					'email'   => $email,
					'phone'   => $phone,
					'session_id'   => $session_id
				)
			);

			$user_id = $wpdb->insert_id;
			$wpdb->insert(
				$tableconversation,
				array(
					'user_id'   => $user_id,
					'conversation'   => $conversation
				)
			);

		}else{

			$user_id = $user_exists->id;
			$wpdb->update(
				$tableuser,
				array(
					'date'  => current_time( 'mysql' ),
					'name'=>$name,
					'email' => $email,
					'phone' => $phone,
				),
				array('id'=>$user_id),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
				),
				array('%d')
			);

			$wpdb->update(
				$tableconversation,
				array(
					'conversation' => $conversation,
				),
				array('user_id'=>$user_id),
				array(
					'%s',
				),
				array('%d')
			);
			
		}


		echo json_encode($response);

		die();
	}
}
add_action( 'wp_ajax_qcld_wb_chatbot_conversation_save',        'qcld_wb_chatbot_conversation_save' );
add_action( 'wp_ajax_nopriv_qcld_wb_chatbot_conversation_save', 'qcld_wb_chatbot_conversation_save' );
}
