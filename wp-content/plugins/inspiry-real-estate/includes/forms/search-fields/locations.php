<?php
global $inspiry_options, $field_counter;
// number of locations chosen from theme options
$location_select_count = ire_get_locations_number();
$location_select_names = ire_get_location_select_names();
$location_titles = ire_get_location_titles( $location_select_count );

// override select boxes titles based on theme options data
for ( $i = 1; $i <= $location_select_count; $i++  ) {
    $temp_location_title = $inspiry_options[ 'inspiry_search_location_title_' . $i  ];
    if( !empty( $temp_location_title ) ) {
        $location_titles[ $i - 1 ] = $temp_location_title;
    }
}?>

<script>
function searchChildSelect(value, elem, toplevel, initiatordebug) {
	var bracket = '{';
	if (elem != undefined) {
		if (!elem.jquery) {
			elem = $('#' + elem);
		}
		if (elem.attr('id') == undefined) {
			return;
		}
		if ($('#' + elem.attr('id')).prop("data-loaded") != true) {
    		$('#' + elem.attr('id')).prop("data-loaded", true);
    		if (toplevel) elem.find('option[value!=0]').remove();
    		const locHttp = new XMLHttpRequest();
    		locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/multiplePostInfo.php?post_type=jurisdiction&meta_key=IDXGenerator_parent_jurisdiction_slug&meta_value=" + encodeURIComponent(value) + "&filter=id|permalink|title|IDXGenerator_jurisdiction_type|IDXGenerator_jurisdiction_name&sortasc=title");
    		locHttp.send();
    		locHttp.onreadystatechange = function() {
    			if (this.readyState == 4 && this.status == 200) {
    				if ((this.responseText) && (this.responseText.indexOf(bracket) != -1)) {
    				    var keepOnChange = $('#' + elem.attr('id')).attr('onchange');
    				    // $('#' + elem.attr('id')).removeAttr('onchange');
    					var useResponse = this.responseText.substring(this.responseText.indexOf(bracket));
    					// console.log(((initiatordebug != undefined)?initiatordebug:"") + "Response for multiplePostInfo (" + elem.attr('id') + "): " + useResponse);
    					var response = jQuery.parseJSON(useResponse);
    					var wasset = false;
    					if (response != null) {
    						if (response.result != undefined) {
    							response.result.forEach(function(entry) {
    								var value = entry.permalink;
    								value = value.replace("/jurisdictions/", "");
    								if (value.endsWith("/")) {
    									value = value.substr(0, value.length - 1);
    								}
    								var option = new Option(entry.title, entry.IDXGenerator_jurisdiction_name);
    								elem.append(option);
    								option.setAttribute("id", value);
    								$('#' + value).prop("jurisid", value);
    								if (entry.IDXGenerator_jurisdiction_type === 'CITY') {
    									searchChildSelect(value, elem, false, initiatordebug);
    								}
    							});
    							if ($('#' + elem.attr('id')).attr('data-delayed-select') != undefined) {
    								if ($('#' + elem.attr('id')).find("option:contains('" + $('#' + elem.attr('id')).attr('data-delayed-select') +"')").length > 0) {
    									if ($('#' + elem.attr('id')).find("option:selected").text().trim() != $('#' + elem.attr('id')).find("option:contains('" + $('#' + elem.attr('id')).attr('data-delayed-select') +"')")) {
    										$('#' + elem.attr('id')).find("option:contains('" + $('#' + elem.attr('id')).attr('data-delayed-select') +"')").prop("selected", true).change(); 
    									}
    									$('#' + elem.attr('id')).removeAttr('data-delayed-select');
    									wasset = true;
    								}
    							}
    						}
    					}
    					if ((!wasset) && (toplevel)) {
    						elem.find('option[value="0"]').attr("selected",true);
    						elem.val('0').change();
    					}
    					$('#' + elem.attr('id')).attr('onchange', keepOnChange);
    				}
    			}
    		}
    	}
	}
}
</script>

<div class="option-bar property-location">
	<select id="Country" name="Country" data-title="Country" class="search-select" onchange="$(this).removeAttr('data-delayed-select'); searchChildSelect($(this).children('option:selected').prop('jurisid'), 'State', true, 'inline')">
		<option value="0">Country (Any)</option>
	</select>
</div>

<div class="option-bar property-location">
	<select id="State" name="State" data-title="State" class="search-select" onchange="$(this).removeAttr('data-delayed-select'); searchChildSelect($(this).children('option:selected').prop('jurisid'), 'City', true, 'inline')">
		<option value="0">Province/State (Any)</option>
	</select>
</div>

<div class="option-bar property-location">
	<select id="City" name="City" data-title="City" class="search-select" onchange="$(this).removeAttr('data-delayed-select');">
		<option value="0">City or Sector (Any)</option>
	</select>
</div>
						
				

<?php
// Generate required location select boxes
 /*for( $i=0; $i < $location_select_count; $i++ ) {
    ?>
    <div class="option-bar property-location">
        <select name="<?php echo esc_attr( $location_select_names[$i] ); ?>" id="<?php echo esc_attr( $location_select_names[$i] );  ?>" data-title="<?php echo esc_attr( $location_titles[$i] ); ?>" class="search-select">
            <option value="any"><?php echo  esc_html( $location_titles[$i] ) . ' ' . esc_html__( '(Any)', 'inspiry-real-estate' ); ?></option>
        </select>
    </div>
    <?php

    if( ( $field_counter + $i ) === 2 && ( ( $field_counter + $location_select_count ) !== 3  ) ) {
	    ire_get_template_part( 'includes/forms/search-fields/hidden-fields-separator' );
    }
}*/



/* important action hook - related JS works based on it */
do_action( 'inspiry_after_location_fields' );

?>