<div class="callCenterCont">
	<div class="dflex callCenterTitle">
		<div class="heading">
		</div>
		<div class="shortcut">
			<div class="fieldCont ">
				<div class="ui-widget">
					<label>Shortcut</label> 
					<select id="combobox">
						<option value="3803">Select Agent</option>
						<option value="440835">Agent 1</option>
						<option value="440836">Agent 2</option>
						<option value="440837">Agent 3</option>
						<option value="440838">Agent 4</option>
						<option value="440839">Agent 5</option>
						<option value="440841">Agent 6</option>
						<option value="440842">Agent 7</option>
						<option value="440843">Agent 8</option>
						<option value="440844">Agent 9</option>
						<option value="440868">Agent 10</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.loading {
		background: url(<?php echo esc_url( IRE_PLUGIN_URL ); ?>public/images/loading3.gif) no-repeat right center;
	}
</style>
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="fieldCont">
			<label>Country</label> 
			<select id="Country" name="Country" onchange="setChildSelect(this.value, 0, 'STATEORPROVINCE', 'State')">
				<option value="0">Select Country</option>
				<?php 
				$jurisdictions_country__args = array(
					'post_type' => 'jurisdiction',
					'posts_per_page' => - 1,
					'meta_query' => array(
						array(
							'key' => 'IDXGenerator_jurisdiction_type',
							'value' => 'COUNTRY',
							'compare' => '=',
							'type' => 'text'
						)
					)
				);
				$countries_query = new WP_Query(apply_filters('inspiry_home_properties', $jurisdictions_country__args));
				usort($countries_query->posts, 'compare_title');
				while ($countries_query->have_posts()) {
					$countries_query->the_post();
					$value = get_permalink();
					$site = site_url('', 'http');
					$value = str_replace($site, "", $value);
					$site = site_url('', 'https');
					$value = str_replace($site, "", $value);
					$value = str_replace("/jurisdictions/", "", $value);
					if (endsWith($value, "/")) {
						$value = substr($value, 0, strlen($value)-1);
					}
					?>
					<option value="<?php echo $value ?>"><?php echo get_the_title() ?></option>
					<?php
				}
				wp_reset_postdata();
				?>
			</select>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="fieldCont">
			<label>State</label> 
			<select id="State" name="State" onchange="setChildSelect(this.value, 1, 'CITY', 'City')">
				<option value="0">Select State</option>
			</select>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="fieldCont">
			<label>City</label> 
				<select id="City" name="City" onchange="setChildSelect(this.value, 2, 'SECTOR', 'Sector')">
				<option value="0">Select City</option>
			</select>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="fieldCont">
			<label>Sector</label> 
				<select id="Sector" name="Sector" onchange="setChildSelect(this.value, 3)">
				<option value="0">Select Sector</option>
			</select>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="fieldCont">
			<label for="language">Language</label> 
			<select id="language"
				name="language">
				<option value="">Any</option>
				<option value="en">English</option>
				<option value="es">Spanish</option>
				<option value="fr">French</option>
			</select>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="fieldCont">
			<div class="form-check">
				<input type="checkbox" class="form-check-input" id="musthavephonenumber" checked>
				<label class="form-check-label" for="musthavephonenumber">Must have Phone-Number</label>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="float-right">
			<button class="btn btn-primary btn-lg" id="btnAgentView" disabled onclick="populateNewAgent()">View</button>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
	<hr class="divider"/>
	</div>
	<script>
		var postmeta = undefined;
		var jurisdictions = [];
		jurisdictions[0] = undefined;
		jurisdictions[1] = undefined;
		jurisdictions[2] = undefined;
		jurisdictions[3] = undefined;
		var agentid = undefined;
		var agentServerContent = undefined;
		var agentLocalContent = undefined;
		var callCenterAgentId = <?php echo wp_get_current_user()->ID; ?>;
		var callCenterAgentName = "<?php echo wp_get_current_user()->user_login; ?>";
		var agentPostId = undefined;
		var filterQueryParams = "&filter=id|IDXGenerator_realEstateBrokerId|IDXGenerator_name|IDXGenerator_phone|IDXGenerator_email|IDXGenerator_firstDetected|IDXGenerator_lastDetected|IDXGenerator_valuation|IDXGenerator_duplicateCount|IDXGenerator_notDuplicateCount|IDXGenerator_urls|CallCenterSecretUrl|CallCenterNotes|CallCenterAssignedAgent|CallCenterAssignedDate|CallCenterCorrectedName|CallCenterCorrectedPhone|CallCenterCorrectedEmail|CallCenterCorrectedWhatsapp|CallCenterLogs|IDXGenerator_language|CallCenter_prefill_profile_image_id|CallCenter_prefill_profile_image_url|CallCenter_prefill_profile_bio|CallCenter_prefill_profile_first_name|CallCenter_prefill_profile_last_name|CallCenter_prefill_profile_user_name|CallCenter_prefill_profile_display_name|CallCenter_prefill_profile_phone|CallCenter_prefill_profile_email|CallCenter_prefill_profile_office_number|CallCenter_prefill_profile_facebook_url|CallCenter_prefill_profile_instagram_url|CallCenter_prefill_profile_linkedin_url|CallCenter_prefill_profile_twitter_url";
		var visitedAgentIds = [];
		function setChildSelect(value, jurisdictions_index, childtype, childid) {
			jurisdictions[jurisdictions_index] = undefined;
			console.log("setChildSelect called: " + value + ", " + jurisdictions_index + ", " + childtype + ", " + childid);
			if (childid != undefined) {
				$('#' + childid).find('option[value!=0]').remove();
				var locHttp = new XMLHttpRequest();
				locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/multiplePostInfo.php?post_type=jurisdiction&meta_key=IDXGenerator_parent_jurisdiction_slug&meta_value=" + encodeURIComponent(value) + "&filter=id|permalink|title");
				locHttp.send();
				locHttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						if ((this.responseText) && (this.responseText.indexOf("{") != -1)) {
							var useResponse = this.responseText.substring(this.responseText.indexOf("{"));
							console.log("Response for multiplePostInfo: " + useResponse);
							var response = jQuery.parseJSON(useResponse);
							var wasset = false;
							if (response != null) {
								if (response.result != undefined) {
									response.result.forEach(function(entry) {
										var value2 = entry.permalink;
										value2 = value2.replace("/jurisdictions/", "");
										if (value2.endsWith("/")) {
											value2 = value2.substr(0, value2.length - 1);
										}
										$('#' + childid).append(new Option(entry.title, value2));
									});
									wasset = true;
								}
							}
							console.log("resetting");
							$('#' + childid).find('option[value="0"]').attr("selected",true);
							$('#' + childid).val('0').change();
							agentInfoEnabling();
						} else if (this.responseText) {
							console.log("Response for multiplePostInfo (unprocessed): " + this.responseText);
						}
					}
				};
			}
			if (value != 0) {
				var locHttp = new XMLHttpRequest();
				locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/postInfo.php?post_type=jurisdiction&meta_key=IDXGenerator_jurisdiction_slug&meta_value=" + encodeURIComponent(value) + "&filter=IDXGenerator_jurisdiction_name");
				locHttp.send();
				locHttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						if ((this.responseText) && (this.responseText.indexOf("{") != -1)) {
							var useResponse = this.responseText.substring(this.responseText.indexOf("{"));
							console.log("Response for postInfo: " + useResponse);
							var response = jQuery.parseJSON(useResponse);
							var wasset = false;
							if (response != null) {
								jurisdictions[jurisdictions_index] = response.IDXGenerator_jurisdiction_name;
								agentInfoEnabling();
							}
						} else if (this.responseText) {
							console.log("Response for postInfo (unprocessed): " + this.responseText);
						}
					}
				};
			}
		}
		
		function sendWhatsappMsg(evt) {
			if (ensureListingAgentAssignedToMe()) {
				console.log("Sending whatsapp message '" + evt.target.text + "' to " + $('#agent_Whatsapp').val() + ".");
				var url = "https://wa.me/" + $('#agent_Whatsapp').val() + "?text=" + encodeURIComponent(evt.target.text);
				var win = window.open(url, '_blank');
				win.focus();
			}
		}
		
		function filterMetaString(content) {
			var dreturn = content;
			dreturn = dreturn.replace('$+NAME+$', $('#agent_Name').val());
			dreturn = dreturn.replace('$+SECRETURL+$', $('#agent_Url').val());
			return dreturn;
		}
		
		function refreshWhatsAppMessages(lang) {
			$('#agent_WhatsappMessages').html('');
			if (lang == 'en') {
				$('#agent_WhatsappMessages').append('<h2>Opening</h2>');
				$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Hello') + '</a>');
				if ($('#agent_Name').val() != '') {
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Hi $+NAME+$') + '</a>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Hello $+NAME+$, how are you?') + '</a>');
					$('#agent_WhatsappMessages').append('<div role="separator" class="dropdown-divider"></div>');
				}
				if ($('#agent_Url').val() != '') {
					$('#agent_WhatsappMessages').append('<h2>Secret-url</h2>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Here is the url to access your page: $+SECRETURL+$') + '</a>');
					$('#agent_WhatsappMessages').append('<div role="separator" class="dropdown-divider"></div>');
				}
				$('#agent_WhatsappMessages').append('<h2>Civilities</h2>');
				$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">Remember that I am here if you need anything related to EveryListing.com!</a>');
				$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">How is business? Good?</a>');
				$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">Have you had the opportunity to look at EveryListing.com?</a>');
			} else if (lang == 'es') {
				if ($('#agent_Name').val() != '') {
					$('#agent_WhatsappMessages').append('<h2>Opening</h2>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Hola $+NAME+$') + '</a>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Hola $+NAME+$, como estas?') + '</a>');
					$('#agent_WhatsappMessages').append('<div role="separator" class="dropdown-divider"></div>');
				}
				if ($('#agent_Url').val() != '') {
					$('#agent_WhatsappMessages').append('<h2>Secret-url</h2>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Aqui es el enlace por su pagina: $+SECRETURL+$') + '</a>');
					$('#agent_WhatsappMessages').append('<div role="separator" class="dropdown-divider"></div>');
				}
			} else if (lang == 'fr') {
				if ($('#agent_Name').val() != '') {
					$('#agent_WhatsappMessages').append('<h2>Opening</h2>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Bonjour $+NAME+$') + '</a>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Bonjour $+NAME+$, comment allez-vous?') + '</a>');
					$('#agent_WhatsappMessages').append('<div role="separator" class="dropdown-divider"></div>');
				}
				if ($('#agent_Url').val() != '') {
					$('#agent_WhatsappMessages').append('<h2>Secret-url</h2>');
					$('#agent_WhatsappMessages').append('<a class="dropdown-item" onclick="sendWhatsappMsg();" href="#">' + filterMetaString('Voici le lien pour la page: $+SECRETURL+$') + '</a>');
					$('#agent_WhatsappMessages').append('<div role="separator" class="dropdown-divider"></div>');
				}
			}
			if ($('#agent_WhatsappMessages').children().last().nodeName == 'div') {
				$('#agent_WhatsappMessages').children().last().remove();
			}
		}
		
		function agentInfoEnabling() {
			var forceEnable = <?php if ($is_secret_url) { echo 'true;'; } else { echo 'false;'; } ?>
			if (agentid == undefined) {
				var x = document.getElementById("agent_prefill");
				if (x.style.display !== "none") {
					myCollapseprefillFunction();
				}
			}
			$('[id ^= agent_').prop("disabled", ((agentid == undefined) && (!forceEnable)));
			$('[id ^= prefill_').prop("disabled", ((agentid == undefined) && (!forceEnable)));
			$('#agent_Skip').css("visibility", agentid == undefined?"none":"visible");
			$('#agent_Save').css("visibility", agentid == undefined?"none":"visible");
			$('#agent_Revert').css("visibility", agentid == undefined?"none":"visible");
			$('[id ^= wells_').css("visibility", agentid == undefined?"hidden":"visible");
			$('#btnAgentView').prop("disabled", (jurisdictions.length == 0) || (jurisdictions[0] == undefined));
			// $('#wellssection').css("visibility", agentid == undefined?"hidden":"visible");
			$('#assignment').css("visibility", agentid == undefined?"none":"visible");
			if (agentid == undefined) {
				$('[id ^= agent_default').css("visibility", "hidden");
				$('#agent_generateButton_secreturl').css("visibility", "hidden");
			} else {
				$('[id ^= agent_default').css("visibility", "visible");
				$('#agent_generateButton_secreturl').css("visibility", "visible");
			}
			manageAllDefaultVisibility();
		}
		
		function manageDefaultVisibility(idText, idButton, objOriginal) {
			var objChanged = $(idText).val();
			if (isSame(objChanged, objOriginal)) {
				$(idButton).css("visibility", "hidden");
			} else {
				$(idButton).css("visibility", "visible");
			}
		}
		
		function manageAllDefaultVisibility() {
			if (agentServerContent == undefined) {
				return;
			}
			manageDefaultVisibility('#agent_Name', '#agent_defaultButton_Name', agentServerContent.IDXGenerator_name);
			manageDefaultVisibility('#agent_Phone', '#agent_defaultButton_Phone', agentServerContent.IDXGenerator_phone);
			manageDefaultVisibility('#agent_Email', '#agent_defaultButton_Email', agentServerContent.IDXGenerator_email);
			manageDefaultVisibility('#agent_Whatsapp', '#agent_defaultButton_Whatsapp', agentServerContent.IDXGenerator_phone);
			if (($('#agent_Url').val() == undefined) || ($('#agent_Url').val() == '')) {
				$('#agent_generateButton_secreturl').css("visibility", "visible");
			} else {
				$('#agent_generateButton_secreturl').css("visibility", "hidden");
			}
		}
		
		function saveListingAgentState() {
			var postmeta = new Object();
			acquirePrefillData(postmeta);
			var locHttp = new XMLHttpRequest();
			var url = "/wp-content/themes/inspiry-real-places/inc/util/savePostMeta.php?id=" + agentPostId;
			url += "&" + encodeURIComponent("CallCenterCorrectedName") + "=" + encodeURIComponent(agentLocalContent.CallCenterCorrectedName);
			url += "&" + encodeURIComponent("CallCenterCorrectedPhone") + "=" + encodeURIComponent(agentLocalContent.CallCenterCorrectedPhone);
			url += "&" + encodeURIComponent("CallCenterCorrectedEmail") + "=" + encodeURIComponent(agentLocalContent.CallCenterCorrectedEmail);
			url += "&" + encodeURIComponent("CallCenterCorrectedWhatsapp") + "=" + encodeURIComponent(agentLocalContent.CallCenterCorrectedWhatsapp);
			url += "&" + encodeURIComponent("CallCenterNotes") + "=" + encodeURIComponent(agentLocalContent.CallCenterNotes);
			url += "&" + encodeURIComponent("IDXGenerator_realEstateBrokerId")  + "=" + encodeURIComponent(agentServerContent.IDXGenerator_realEstateBrokerId);
			if (agentLocalContent.CallCenterSecretUrl != null) {
				url += "&" + encodeURIComponent("CallCenterSecretUrl") + "=" + encodeURIComponent(agentLocalContent.CallCenterSecretUrl.replace("<?php echo site_url('/', 'https') ?>", "/").replace("<?php echo site_url('/', 'http') ?>", "/"));
			}
			url += "&" + encodeURIComponent("CallCenterAssignedAgent") + "=" + encodeURIComponent(agentLocalContent.CallCenterAssignedAgent);
			for (var key in postmeta) {
				if (key.startsWith("prefill_")) {
					agentServerContent["CallCenter_" + key] = postmeta[key];
					url += "&" + encodeURIComponent("CallCenter_" + key) + "=" + encodeURIComponent(postmeta[key]);
				}
			}
			locHttp.open("GET", url);
			locHttp.send();
			locHttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					console.log("Response for getListingAgentInfo: " + this.responseText);
					if (this.responseText) {
						locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/getListingAgentInfo.php?id=" + agentPostId + filterQueryParams);
						locHttp.send();
						locHttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
								if ((this.responseText) && (this.responseText.indexOf("{") != -1)) {
									var useResponse = this.responseText.substring(this.responseText.indexOf("{"));
									useResponse = useResponse.replace("\"undefined\"", "\"\"");
									console.log("Response for getListingAgentInfo: " + useResponse);
									agentServerContent = jQuery.parseJSON(useResponse);
									if (agentServerContent != null) {
										agentLocalContent = jQuery.parseJSON(useResponse);
										agentid = agentServerContent.IDXGenerator_realEstateBrokerId;
										visitedAgentIds.push(agentid);
										agentPostId = agentServerContent.id;
										refreshUIWithListingAgentContent();
									} else {
										clearDisplayedAgent();
									}
								} else if (this.responseText) {
									console.log("Response for getListingAgentInfo (unprocessed): " + this.responseText);
									clearDisplayedAgent();
								}											
							}
						}
					}											
				}
			}												
		}
		
		function ensureListingAgentAssignedToMe() {
			if (agentLocalContent.CallCenterAssignedAgent == callCenterAgentId) {
				return true;
			} else if (agentLocalContent.CallCenterAssignedAgent == undefined) {
				agentLocalContent.CallCenterAssignedAgent = callCenterAgentId;
				saveListingAgentState();
				return true;
			} else {
				return false;
			}
		}
		
		function refreshUIWithListingAgentContent() {
			if (agentServerContent != undefined) {
				var postmeta = new Object();
				postmeta['prefill_profile_bio'] = agentServerContent.CallCenter_prefill_profile_bio;
				postmeta['prefill_profile_image_id'] = agentServerContent.CallCenter_prefill_profile_image_id;
				postmeta['prefill_profile_image_url'] = agentServerContent.CallCenter_prefill_profile_image_url;											
				postmeta['prefill_profile_first_name'] = agentServerContent.CallCenter_prefill_profile_first_name;
				postmeta['prefill_profile_last_name'] = agentServerContent.CallCenter_prefill_profile_last_name;
				postmeta['prefill_profile_user_name'] = agentServerContent.CallCenter_prefill_profile_user_name;
				postmeta['prefill_profile_display_name'] = agentServerContent.CallCenter_prefill_profile_display_name;
				postmeta['prefill_profile_office_number'] = agentServerContent.CallCenter_prefill_profile_office_number;
				postmeta['prefill_profile_facebook_url'] = agentServerContent.CallCenter_prefill_profile_facebook_url;
				postmeta['prefill_profile_instagram_url'] = agentServerContent.CallCenter_prefill_profile_instagram_url;
				postmeta['prefill_profile_linkedin_url'] = agentServerContent.CallCenter_prefill_profile_linkedin_url;
				postmeta['prefill_profile_twitter_url'] = agentServerContent.CallCenter_prefill_profile_twitter_url;
			}
			if (agentServerContent != undefined) {
				$('#wells_valuationtext').text(agentServerContent.IDXGenerator_valuation); 
			} else {
				$('#wells_valuationtext').text(''); 
			}
			if (agentServerContent != undefined) {
				$('#wells_urlstext').html("<notextile></notextile><span>" + agentServerContent.IDXGenerator_urls + "</span>");
				$('#wells_urlstext').prop("onclick", undefined).off("click");
			} else {
				$('#wells_urlstext').text(''); 
			}
			if (agentServerContent != undefined) {
				if ((agentServerContent.CallCenterCorrectedName != undefined) && (agentServerContent.CallCenterCorrectedName.length > 0)) {
					$('#agent_Name').val(agentServerContent.CallCenterCorrectedName);
				} else {
					$('#agent_Name').val(agentServerContent.IDXGenerator_name);
				}
			} else {
				$('#agent_Name').val('');
			}
			if (agentServerContent != undefined) {
				if ((agentServerContent.CallCenterCorrectedPhone != undefined) && (agentServerContent.CallCenterCorrectedPhone.length > 0)) {
					$('#agent_Phone').val(agentServerContent.CallCenterCorrectedPhone);
					postmeta['prefill_profile_phone'] = agentServerContent.CallCenterCorrectedPhone;
				} else {
					$('#agent_Phone').val(agentServerContent.IDXGenerator_phone);
					postmeta['prefill_profile_phone'] = agentServerContent.IDXGenerator_phone;
				}
			} else {
				$('#agent_Phone').val('');
			}
			if (agentServerContent != undefined) {
				if ((agentServerContent.CallCenterCorrectedEmail != undefined) && (agentServerContent.CallCenterCorrectedEmail.length > 0)) {
					$('#agent_Email').val(agentServerContent.CallCenterCorrectedEmail);
					postmeta['prefill_profile_email'] = agentServerContent.CallCenterCorrectedEmail;
				} else {
					$('#agent_Email').val(agentServerContent.IDXGenerator_email);
					postmeta['prefill_profile_email'] = agentServerContent.IDXGenerator_email;
				}
			} else {
				$('#agent_Email').val('');
			}
			if (agentServerContent != undefined) {
				if ((agentServerContent.CallCenterCorrectedWhatsapp != undefined) && (agentServerContent.CallCenterCorrectedWhatsapp.length > 0)) {
					$('#agent_Whatsapp').val(agentServerContent.CallCenterCorrectedWhatsapp);
				} else if ((agentServerContent.CallCenterCorrectedPhone != undefined) && (agentServerContent.CallCenterCorrectedPhone.length > 0)) {
					$('#agent_Whatsapp').val(agentServerContent.CallCenterCorrectedPhone);
				} else {
					$('#agent_Whatsapp').val(agentServerContent.IDXGenerator_phone);
				}
			} else {
				$('#agent_Whatsapp').val('');
			}
			if (agentServerContent != undefined) {
				if ((agentServerContent.CallCenterNotes != undefined) && (agentServerContent.CallCenterNotes.length > 0)) {
					$('#agent_Notes').val(agentServerContent.CallCenterNotes);
				} else {
					$('#agent_Notes').val('');
				}
			} else {
				$('#agent_Notes').val('');
			}
			if (agentServerContent != undefined) {
				if (agentServerContent.CallCenterSecretUrl != undefined) {
					$('#agent_Url').val(("<?php echo site_url('', 'https') ?>" + agentServerContent.CallCenterSecretUrl).replace("https:", location.protocol));
				} else {
					$('#agent_Url').val('');
				}
			} else {
				$('#agent_Url').val('');
			}
			if (agentServerContent != undefined) {
				$('#wells_FirstDetected').text(agentServerContent.IDXGenerator_firstDetected);
				$('#wells_LastDetected').text(agentServerContent.IDXGenerator_lastDetected);
				$('#wells_NonDuplicateCount').text(agentServerContent.IDXGenerator_notDuplicateCount);
				$('#wells_DuplicateCount').text(agentServerContent.IDXGenerator_duplicateCount);
			}
			if (agentServerContent != undefined) {
				if (agentServerContent.CallCenterAssignedAgent == callCenterAgentId) {
					$('#assignment').removeClass('alert-danger').removeClass('alert-warning').removeClass('alert-success').addClass('alert-success');
					if (agentServerContent.CallCenterAssignedDate != undefined) {
						$('#info_assignment').text('Listing-agent assigned to you since ' + agentServerContent.CallCenterAssignedDate);
					} else {
						$('#info_assignment').text('Listing-agent assigned to you');
					}
				} else if ((agentServerContent.CallCenterAssignedAgent != undefined) && (agentServerContent.CallCenterAssignedAgent.length > 0)) {
					$('#assignment').removeClass('alert-danger').removeClass('alert-warning').removeClass('alert-success').addClass('alert-danger');
					if (agentServerContent.CallCenterAssignedDate != undefined) {
						$('#info_assignment').text('Listing-agent assigned to someone else since ' + agentServerContent.CallCenterAssignedDate);
					} else {
						$('#info_assignment').text('Listing-agent assigned to someone else');
					}
				} else {
					$('#assignment').removeClass('alert-danger').removeClass('alert-warning').removeClass('alert-success').addClass('alert-warning');
					$('#info_assignment').text('Listing-agent not assigned yet');
				}
			} else {
				$('#assignment').removeClass('alert-danger').removeClass('alert-warning').removeClass('alert-success').removeClass('alert-warning');
			}
			agentInfoEnabling();
			if (agentServerContent != undefined) grepAllFromUI();
			if (agentServerContent != undefined) manageAllDefaultVisibility(true);
			if (agentServerContent != undefined) populatePrefillData(postmeta);
		}
		
		function isSame(obj1, obj2) {
			return ((obj1 === obj2) || (((obj1 == '') || (obj1 == undefined)) && ((obj2 == '') || (obj2 == undefined))));
		}
		
		function isAgentInformationDirty() {
			var dReturn = false;
			dReturn |= (!isSame(agentServerContent.CallCenterSecretUrl, agentLocalContent.CallCenterSecretUrl));
			dReturn |= (!isSame(agentServerContent.CallCenterNotes, agentLocalContent.CallCenterNotes));
			dReturn |= (!isSame(agentServerContent.CallCenterAssignedAgent, agentLocalContent.CallCenterAssignedAgent));
			dReturn |= (!isSame(agentServerContent.CallCenterCorrectedName, agentLocalContent.CallCenterCorrectedName));
			dReturn |= (!isSame(agentServerContent.CallCenterCorrectedPhone, agentLocalContent.CallCenterCorrectedPhone));
			dReturn |= (!isSame(agentServerContent.CallCenterCorrectedEmail, agentLocalContent.CallCenterCorrectedEmail));
			dReturn |= (!isSame(agentServerContent.CallCenterCorrectedWhatsapp, agentLocalContent.CallCenterCorrectedWhatsapp));
			return dReturn;
		}
		
		function grepAllFromUI() {
			agentLocalContent.CallCenterSecretUrl = $('#agent_Url').val();
			agentLocalContent.CallCenterNotes = $('#agent_Notes').val();
			agentLocalContent.CallCenterCorrectedName = ($('#agent_defaultButton_Name').css('visibility') == 'visible')?$('#agent_Name').val():undefined;
			agentLocalContent.CallCenterCorrectedPhone = ($('#agent_defaultButton_Phone').css('visibility') == 'visible')?$('#agent_Phone').val():undefined;
			agentLocalContent.CallCenterCorrectedEmail = ($('#agent_defaultButton_Email').css('visibility') == 'visible')?$('#agent_Email').val():undefined;
			agentLocalContent.CallCenterCorrectedWhatsapp = ($('#agent_defaultButton_Whatsapp').css('visibility') == 'visible')?$('#agent_Whatsapp').val():undefined;
			$('#agent_Save').prop('disabled', !isAgentInformationDirty());
			$('#btnAgentView').prop('disabled', isAgentInformationDirty());
			$('#agent_Skip').prop('disabled', isAgentInformationDirty());
			$('#agent_Revert').prop('disabled', !isAgentInformationDirty());
			refreshWhatsAppMessages(agentLocalContent.IDXGenerator_language);
		}
		
		function populateNewAgent() {
			console.log("Pick random listing-agent from: " + jurisdictions[0] + (((jurisdictions.length > 0) && (jurisdictions[1] != undefined))?(", " + jurisdictions[1]):"") + (((jurisdictions.length > 1) && (jurisdictions[2] != undefined))?(", " + jurisdictions[2]):"") + (((jurisdictions.length > 2) && (jurisdictions[3] != undefined))?(", " + jurisdictions[3]):""));
			var customField;
			var valueOfCustomField;
			if ((jurisdictions.length > 2) && (jurisdictions[3] != undefined)) {
				customField = "IDXGenerator_Sector";
				valueOfCustomField = jurisdictions[3];
			} else if ((jurisdictions.length > 1) && (jurisdictions[2] != undefined)) {
				customField = "IDXGenerator_City";
				valueOfCustomField = jurisdictions[2];
			} else if ((jurisdictions.length > 0) && (jurisdictions[1] != undefined)) {
				customField = "IDXGenerator_Province";
				valueOfCustomField = jurisdictions[1];
			} else {
				customField = "IDXGenerator_Country";
				valueOfCustomField = jurisdictions[0];
			}
			var locHttp = new XMLHttpRequest();
			locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/getListingAgentInfo.php?post_type=property&meta_key=" + customField + ((($('#language').val() != undefined) && ($('#language').val() != ''))?("&language=" + $('#language').val()):"") +
			"&meta_value=" + encodeURIComponent(valueOfCustomField) + "&random=1&visited=" + encodeURIComponent(JSON.stringify(visitedAgentIds)) + "&mustphone=" + $('#musthavephonenumber').prop('checked') + filterQueryParams);
			locHttp.send();
			locHttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if ((this.responseText) && (this.responseText.indexOf("{") != -1)) {
						var useResponse = this.responseText.substring(this.responseText.indexOf("{"));
						useResponse = useResponse.replace("\"undefined\"", "\"\"");
						console.log("Response for getListingAgentInfo: " + useResponse);
						agentServerContent = jQuery.parseJSON(useResponse);
						if (agentServerContent != null) {
							agentLocalContent = jQuery.parseJSON(useResponse);
							agentid = agentServerContent.IDXGenerator_realEstateBrokerId;
							visitedAgentIds.push(agentid);
							agentPostId = agentServerContent.id;
							refreshUIWithListingAgentContent();
						} else {
							clearDisplayedAgent();
						}
					} else if (this.responseText) {
						console.log("Response for getListingAgentInfo (unprocessed): " + this.responseText);
						clearDisplayedAgent();
					}											
				}
			}												
		}
		
		function revertToSaved() {
			agentLocalContent = jQuery.parseJSON(JSON.stringify(agentServerContent));
			refreshUIWithListingAgentContent();
			manageAllDefaultVisibility();
			grepAllFromUI();
		}
		
		function clearDisplayedAgent() {
			alert("No agent found with these criteria.");
			agentServerContent = undefined;
			agentLocalContent = undefined;
			agentPostId = undefined;
			agentid = undefined;
			refreshUIWithListingAgentContent();
		}
		
		function generateSecretUrl() {
			var key = undefined;
			var secret;
			if (($('#agent_Name').val() != undefined) && ($('#agent_Name').val() != '')) {
				key = $('#agent_Name').val().replace(" ", "_");
			} else {
				key = '';
			}
			if ((key.length <= 5) && ($('#agent_Phone').val() != undefined) && ($('#agent_Phone').val() != '')) {
				key += (((key != '')?"_":"") + $('#agent_Phone').val());
			}
			if ((key.length <= 5) && ($('#agent_Email').val() != undefined) && ($('#agent_Email').val() != '')) {
				key += (((key != '')?"_":"") + $('#agent_Email').val());
			}
			if (key.length <= 5) {
				key = Math.floor((Math.random()) * 0x10000).toString(16);
			}
			key = key.replace(/[^0-9a-z_]/gi, '');
			key = encodeURIComponent(key);
			secret = Math.floor((Math.random()) * 0x10000).toString(16) + Math.floor((Math.random()) * 0x10000).toString(16);
			secret = encodeURIComponent(secret);
			console.log("key: " + key + ", secret: " + secret);
			var keepCallCenterSecretUrl = agentLocalContent.CallCenterSecretUrl;
			agentLocalContent.CallCenterSecretUrl = "<?php echo site_url('/', 'https') ?>" + "member-overview-page?key=" + key + "&secret=" + secret;
			agentLocalContent.CallCenterSecretUrl = agentLocalContent.CallCenterSecretUrl.replace("https:", location.protocol);
			console.log("secret url: " + agentLocalContent.CallCenterSecretUrl);
			if (!ensureListingAgentAssignedToMe()) {
				agentLocalContent.CallCenterSecretUrl = keepCallCenterSecretUrl;
			}
			
		}
		
		$(document).ready(function () {
			agentInfoEnabling();
		});
	</script>
</div>
<div class="ccDetails">
	<div class="row">
		<div id="assignment" class="col-xs-12 col-sm-12 col-md-12 alert" role="alert">
		<label for="info_assignment">Assignment</label><span id="info_assignment" name="info_assignment"/>
		</div>
		<form id="save_agent" action="" method="POST">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<?php
				$current_user = wp_get_current_user();
			?>
				<label class="float-left" for="agent_Name">Name</label><button class="btn btn-outline-secondary btn-sm float-right" id="agent_defaultButton_Name" type="button" style="padding-bottom: 5px;" onclick="$('#agent_Name').val(agentServerContent.IDXGenerator_name); agentLocalContent.CallCenterCorrectedName = undefined; manageAllDefaultVisibility(); grepAllFromUI();">Default</button>
				<input type="text" id="agent_Name" name="agent_Name" value="" onkeyup="manageAllDefaultVisibility(); grepAllFromUI();"/>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<label class="float-left" for="agent_Phone">Phone-Number</label><button class="btn btn-outline-secondary btn-sm float-right" id="agent_defaultButton_Phone" type="button" style="padding-bottom: 5px;" onclick="$('#agent_Phone').val(agentServerContent.IDXGenerator_phone); agentLocalContent.CallCenterCorrectedPhone = undefined; manageAllDefaultVisibility(); grepAllFromUI();">Default</button>
				<input type="text" id="agent_Phone" name="agent_Phone" onkeyup="manageAllDefaultVisibility(); grepAllFromUI();"/>
			</div>
			<script>
			function mirrorvals(val1, elem2) {
				console.log(val1);
				if ($(elem2).length > 0) {
					$(elem2).val(val1);
				}
			}
			</script>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="fieldCont">
					<label class="float-left" for="agent_Email">Email</label><button class="btn btn-outline-secondary btn-sm float-right" id="agent_defaultButton_Email" type="button" style="padding-bottom: 5px;" onclick="$('#agent_Email').val(agentServerContent.IDXGenerator_email); agentLocalContent.CallCenterCorrectedEmail = undefined; manageAllDefaultVisibility(); grepAllFromUI();">Default</button>
					<input type="text" id="agent_Email" name="agent_Email" onkeyup="manageAllDefaultVisibility(); grepAllFromUI(); mirrorvals($('#agent_Email').val(), '#prefill-email');"/>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="fieldCont">
					<label class="float-left" for="agent_Url">Secret-Url</label><button class="btn btn-outline-secondary btn-sm float-right" id="agent_generateButton_secreturl" type="button" style="padding-bottom: 5px; visibility: hidden;" onclick="generateSecretUrl();">Generate</button>
					<input type="text" id="agent_Url" name="agent_Url" value="" readonly style="cursor: default;"/>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<span class="float-left row">
					<label style="padding-right: 15px;" for="agent_Whatsapp"><img
						src="<?php echo esc_url( IRE_PLUGIN_URL ); ?>public/images/whatsapp.svg"
						width="24" alt="Whatsapp"/>&nbsp;WhatsApp</label>
					<div>
						<button class="btn btn-outline-secondary dropdown-toggle" id="agent_WhatsappAction" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Message</button>
						<div id="agent_WhatsappMessages" class="dropdown-menu">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
							<div role="separator" class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">Separated link</a>
						</div>
					</div>
				</span>
				<button class="btn btn-outline-secondary btn-sm float-right align-text-bottom float-right" id="agent_defaultButton_Whatsapp" type="button" style="padding-bottom: 5px;" onclick="$('#agent_Whatsapp').val(agentServerContent.IDXGenerator_phone); agentLocalContent.CallCenterCorrectedWhatsapp = undefined; manageAllDefaultVisibility(); grepAllFromUI();">Default</button>
				<input type="text" id="agent_Whatsapp" onkeyup="manageAllDefaultVisibility(); grepAllFromUI();"/>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="fieldCont">
					<label>Follow-up (days)</label> <select id="agent_Fallowup"
						name="agent_Fallowup">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>5</option>
						<option>10</option>
						<option>14</option>
						<option>30</option>
						<option>60</option>
						<option>90</option>
						<option>365</option>
					</select>
				</div>
			</div>
			<div id="agent_prefillsection" class="col-xs-12 col-sm-12 col-md-12 well">
				<h3 id="agent_panel_prefill" class="panel-title" style="margin-top: 15px;">
					<a id="agent_collapse_prefill" onclick="myCollapseprefillFunction()" role="button" class="stretched-link" style="text-decoration: none; position: relative;">Pre-filled data&nbsp;<i id="prefill_arrow_icon" class="fa fa-angle-right"></i></a>
				</h3>
				<div id="agent_prefill" style="display: none;">
					<div class="panel-body">
						<?php require_once('general_surl.php'); ?>
					</div>
				</div>
				<script>
					function myCollapseprefillFunction() {
						var x = document.getElementById("agent_prefill");
						if ((agentid != undefined) || (x.style.display !== "none")) {
							if (x.style.display === "none") {
								x.style.display = "block";
								$("#prefill_arrow_icon").removeClass('fa-angle-right').addClass('fa-angle-down');
							} else {
								x.style.display = "none";
								$("#prefill_arrow_icon").addClass('fa-angle-right').removeClass('fa-angle-down');
							}
						}
					};
				</script>
			</div>
			<div id="wellssection" class="col-xs-12 col-sm-12 col-md-12 well" style="margin-top: 10px;">
				<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="wells_FirstDetected">First detected</label><span id="wells_FirstDetected" name="wells_FirstDetected"/></div>
				<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="wells_LastDetected">Last detected</label><span id="wells_LastDetected" name="wells_LastDetected"/></div>
				<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="wells_NonDuplicateCount">Non-duplicate listings count</label><span id="wells_NonDuplicateCount" name="wells_NonDuplicateCount"/></div>
				<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="wells_DuplicateCount">Duplicate listings count</label><span id="wells_DuplicateCount" name="wells_DuplicateCount"/></div>
				<div class="col-xs-12 col-sm-12 col-md-12"
				style="margin-bottom: 15px;">
				<a id="wells_cuslinks" class="text-warning stretched-link" style="cursor: pointer;" onclick="$('#valuationdiv').toggle();">Listing Agent Valuation Information (prices are in USD$)</a>
					<div id="wells_valuationdiv">
						<pre id="wells_valuationtext"></pre>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12"
					style="margin-bottom: 15px;">
					<a id="wells_urllinks" class="text-warning" style="cursor: pointer;" onclick="$('#urlsdiv').toggle();">Urls (prices are in USD$)</a>
					<div id="wells_urlsdiv">
						<pre id="wells_urlstext"></pre>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="fieldCont">
					<label>Notes</label>
					<textarea id="agent_Notes" name="aNotes" onkeyup="grepAllFromUI();"></textarea>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="float-left">
					<button id="agent_Revert" class="btn btn-lg" type="button" onclick="revertToSaved();">Reset to Saved</button>
				</div>
				<div class="float-right">
					<button id="agent_Skip" class="btn btn-lg" type="button" onclick="populateNewAgent();">Skip</button>
					<button id="agent_Save" class="btn btn-secondary btn-lg" type="button" onclick="saveListingAgentState();">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>