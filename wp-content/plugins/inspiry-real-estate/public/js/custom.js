

// A $( document ).ready() block.
$( document ).ready(function() {
    
	    $('body').on('change', '.countries', function() {
			
	
        var countryid = $(this).val();
        if(countryid != '') {
            var data = {
                'action': 'get_states_by_ajax',
                'country': countryid,
                'ajaxurl': "/wp-admin/admin-ajax.php",
            }

            $.post('/wp-admin/admin-ajax.php', data, function(response) {
                $('.states').html(response);
            });
        }
	
		
		/*
		
		 var newCustomerForm = jQuery(this).serialize();

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: newCustomerForm,
        success: function(data){
            jQuery("#feedback").html(data);
        }
    });

    return false;
		
		*/
    });
	
	
});