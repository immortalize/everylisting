<?php
/**
* Plugin Name: LiveChat Addon
* Plugin URI: https://www.quantumcloud.com/products/plugins/
* Description: Live Chat with you customers using the WPBot
* Version: 2.0.0
* Author: QuantumCloud
* Author URI: https://www.quantumcloud.com/
* Requires at least: 4.6
* Tested up to: 5.4
* License: GPL2
*/

if(session_id() == '') {
	@session_start();
}

if( !defined('WBCA_PATH') )
	define( 'WBCA_PATH', plugin_dir_path(__FILE__) );
if( !defined('WBCA_URL') )
	define( 'WBCA_URL', plugin_dir_url(__FILE__ ) );
	
//with trailing slash
if( !defined('WBCA') )
	define( 'WBCA', 'wbca' );
	
add_action( 'wp_footer', 'wbca_sound_function');
add_action( 'admin_footer', 'wbca_sound_function');
add_filter( 'login_redirect', 'wbca_login_redirect', 10, 3 );
require_once 'ajax/class-wbca-ajax.php';
require_once 'ajax/class-wbca-admin-ajax.php';
require_once 'controllers/class-wbca-database-manager.php';
require_once 'admin/class-wbca-options.php';
require('plugin-upgrader/plugin-upgrader.php');

class wbca_Apps {
	
	 public function __construct() {
		 add_action( 'init', array( $this, 'load_wbca_admin') );
		 add_role( 'livechatuser', __( 'Live Chat User' ), array( ) );
    }
	
    public function initialize_controllers() {

        require_once 'controllers/class-wbca-activation-controller.php';
        $activation_controller = new wbca_Activation_Controller();
        $activation_controller->initialize_activation_hooks();
		
		require_once 'controllers/class-wbca-schedule-controller.php';
        $schedule_controller = new wbca_Schedule_Controller();
    }

    public function initialize_app_controllers() {

		require_once 'controllers/class-wbca-script-controller.php';
        $script_controller = new wbca_Script_Controller();
        $script_controller->enque_scripts();

        $ajax = new wbca_Ajax();
        $ajax->initialize();
		
		$admin = new wbca_Admin_Ajax();
        $admin->initialize();

    }
	
	public function load_wbca_admin(){
		require_once 'controllers/class-admin-area-controller.php';
		$admin_init = new wbca_Admin_Area_Controller();
	}
	

}

$wbca_app = new wbca_Apps();
$wbca_app->initialize_controllers();

function wbca_load_wbca(){
	$wbca_init = new wbca_Apps();
	$wbca_init->initialize_app_controllers();
}

add_action('init', 'wbca_load_wbca');


function wbca_sound_function() {
	$sound = '';
	$sound .= '<audio id="wbca_alert" loop="loop">';
	$sound .= '<source src="' . plugins_url() . '/live-chat-addon/images/alert.ogg" type="audio/ogg">';
	$sound .= '<source src="' . plugins_url() . '/live-chat-addon/images/alert.mp3" type="audio/mpeg">';
	$sound .= '</audio>';
	echo $sound;
}

function wbca_login_redirect( $redirect_to, $request, $user ) {
	
	//is there a user to check?
	global $user;
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'operator', $user->roles ) ) {
			// redirect them to the default place
			return admin_url().'admin.php?page=wbca-chat-page';
			
		} else if( in_array( 'subscriber', $user->roles ) ) {
			return home_url();
		}else if(in_array( 'administrator', $user->roles )){
			return admin_url();
		}else if(in_array( 'editor', $user->roles )){
			return admin_url();
		}else{
			return $redirect_to;
		}
	} else {
		return $redirect_to;
	}	
}

add_action('init', 'qcpd_wplivechat_checking_dependencies');
function qcpd_wplivechat_checking_dependencies(){

	
	if ( !class_exists('qcld_wb_Chatbot') && !class_exists('QCLD_Woo_Chatbot') && (qclivechat_is_kbxwpbot_active() != true)) {
		add_action('admin_notices', 'qcpd_wpbot_require_notice');
	}
}



function qcpd_wpbot_require_notice()
{
?>
	<div id="message" class="error">
		<p>
			Please install & activate the WPBot pro or WoowBot pro plugin to get the Livechat Addon to work.
		</p>
	</div>
<?php
}
function qcld_livechat_license_callback(){
	?>
	<div id="licensing">
		<h1>Please Insert your license Key</h1>
		<?php if( get_wplivechat_valid_license() ){ ?>
			<div class="qcld-success-notice">
				<p>Thank you, Your License is active</p>
			</div>
		<?php } ?>
		
		<?php
		
			$track_domain_request = wp_remote_get(wplivechat_LICENSING_PRODUCT_DEV_URL."wp-json/qc-domain-tracker/v1/getdomain/?license_key=".get_wplivechat_licensing_key());
			if( !is_wp_error( $track_domain_request ) || wp_remote_retrieve_response_code( $track_domain_request ) === 200 ){
				$track_domain_result = json_decode($track_domain_request['body']);
				
				$max_domain_num = $track_domain_result[0]->max_domain + 1;
				$total_domains = @json_decode($track_domain_result[0]->domain, true);
				if(!empty($total_domains)){
				$total_domains_num = count($total_domains);

				if( $max_domain_num <= $total_domains_num){
			?>
					<div class="qcld-error-notice">
						<p>You have activated this key for maximum number of sites allowed by your license. Please <a href='https://www.quantumcloud.com/products/'>purchase additional license.</a></p>
					</div>
			<?php
				}
				}
				
			}
		?>
		
		<form onsubmit="return false" id="qc-license-form" method="post" action="options.php">
			<?php
				delete_wplivechat_update_transient();
				delete_wplivechat_renew_transient();
				
				delete_option('_site_transient_update_plugins');
				settings_fields( 'qcld_wplivechat_license' );
				do_settings_sections( 'qcld_wplivechat_license' );

				// if( isset($_POST['submit']) ){
				// 	echo 'qcld_wplivechat_buy_from_where '.$_POST['qcld_wplivechat_buy_from_where'];
				// }
			?>
			<table class="form-table">
				

				<tr id="quantumcloud_portfolio_license_row" style="display: none">
					<th>
						<label for="qcld_wplivechat_enter_license_key">Enter License Key:</label>
					</th>
					<td>
						<input type="<?php echo (get_wplivechat_licensing_key()!=''?'password':'text'); ?>" id="qcld_wplivechat_enter_license_key" name="qcld_wplivechat_enter_license_key" class="regular-text" value="<?php echo get_wplivechat_licensing_key(); ?>">
						<p>You can copy the license key from <a target="_blank" href='https://www.quantumcloud.com/products/account/'>your account</a></p>
					</td>
				</tr>

				<tr id="show_envato_plugin_downloader" style="display: none">
					<th>
						<label for="qcld_wplivechat_enter_envato_key">Enter Purchase Code:</label>
					</th>
					<td colspan="4">
						<input type="<?php echo (get_wplivechat_envato_key()!=''?'password':'text'); ?>" id="qcld_wplivechat_enter_envato_key" name="qcld_wplivechat_enter_envato_key" class="regular-text" value="<?php echo get_wplivechat_envato_key(); ?>">
						<p>You can install the <a target="_blank" href="https://envato.com/market-plugin/">Envato Plugin</a> to stay up to date.</p>
					</td>
				</tr>
				
				<tr>
					<th>
						<label for="qcld_wplivechat_enter_license_or_purchase_key">Enter License Key or Purchase Code:</label>
					</th>
					<td>
						<input type="<?php echo (get_wplivechat_license_purchase_code()!=''?'password':'text'); ?>" id="qcld_wplivechat_enter_license_or_purchase_key" name="qcld_wplivechat_enter_license_or_purchase_key" class="regular-text" value="<?php echo get_wplivechat_license_purchase_code(); ?>" required>
					</td>
				</tr>



			</table>
			<!-- //start new-update-for-codecanyon -->
			<input type="hidden" name="qcld_wplivechat_buy_from_where" value="<?php echo get_wplivechat_licensing_buy_from(); ?>" >
			<!-- //end new-update-for-codecanyon -->
			<?php submit_button(); ?>
		</form>
		<script type="text/javascript">
			jQuery(document).ready(function(){

				//start new-update-for-codecanyon
				jQuery('#qcld_wplivechat_enter_license_or_purchase_key').on('focusout', function(){
					qc_wplivechat_set_plugin_license_fields();
				});

				jQuery('#qcld_wplivechat_enter_license_or_purchase_key').on('keypress',function (e) {
					  qc_wplivechat_set_plugin_license_fields();
				});

				jQuery('#qc-license-form input[type="submit"]').on('click', function(){
					qc_wplivechat_set_plugin_license_fields();
					jQuery('#qc-license-form').removeAttr('onsubmit').submit();
				});

				function qc_wplivechat_set_plugin_license_fields(){
					var license_input = jQuery('#qcld_wplivechat_enter_license_or_purchase_key').val();
					if( /^(\w{8})-((\w{4})-){3}(\w{12})$/.test(license_input) ){
						jQuery('input[name="qcld_wplivechat_buy_from_where"]').val('codecanyon');
						jQuery('input[name="qcld_wplivechat_enter_envato_key"]').val(license_input);
					}else{
						jQuery('input[name="qcld_wplivechat_buy_from_where"]').val('quantumcloud');
						jQuery('input[name="qcld_wplivechat_enter_license_key"]').val(license_input);
					}
				}
				//end new-update-for-codecanyon

			});
		</script>
	</div>
<?php 
}

add_action( 'activated_plugin', 'qc_wplivechat_activation_redirect' );
function qc_wplivechat_activation_redirect($plugin){
	if( $plugin == plugin_basename( __FILE__ ) ) {
		exit( wp_redirect( admin_url( 'admin.php?page=qc-wplive-chat-help-license') ) );
	}
}

add_action( 'admin_menu' , 'qclc_remove_submenu', 20 );
function qclc_remove_submenu(){
	global $menu;
	
	foreach($menu as $key=>$value){
		if($value[2]=='wbca_admin_page'){
			unset($menu[$key]);
		}
	}
	return ($menu);
}
add_action( 'admin_menu' , 'qclc_add_submenu', 30 );
function qclc_add_submenu(){
	global $submenu;
	foreach($submenu['wbca-chat-page'] as $key=>$value){
		if($value[0]=='Live Chat Options'){
			

			unset($submenu['wbca-chat-page'][$key]);
			$submenu['wbca-chat-page'][2] = array( 'Live Chat Options', 'publish_posts' , admin_url('admin.php?page=wbca_admin_page') );
			ksort($submenu['wbca-chat-page']);
			
		}
		
	}
	return $submenu;
}



function qclivechat_is_woowbot_active(){
	if(class_exists('QCLD_Woo_Chatbot')){
		return true;
	}else{
		return false;
	}
}

function qclivechat_is_wpbot_active(){
	if(class_exists('qcld_wb_Chatbot')){
		return true;
	}else{
		return false;
	}
}

function qclivechat_is_kbxwpbot_active(){

	if ( defined( 'KBX_WP_CHATBOT' ) && (KBX_WP_CHATBOT == '1') ) {
		return true;
	}else{
		return false;
	}
}