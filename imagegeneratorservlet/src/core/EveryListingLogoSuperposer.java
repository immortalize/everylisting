package core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import core.AnimationBuilder.Sequence;
import util.BufferedImageUtils;

public class EveryListingLogoSuperposer extends FrameSuperposer {

	private ELocation location;
	private int size;
	private int padding;
	private transient BufferedImage logo;

	@Override
	public Rectangle render(BufferedImage image, Dimension dimension, Rectangle notSuperposedRect, AnimationBuilder caller, Sequence sequence) throws Exception {
		float originalProportions = ((float)logo.getHeight() / (float)logo.getWidth());
		Rectangle destRect = new Rectangle();
		destRect.width = (dimension.width * size)/100;
		destRect.height = (int) ((float)destRect.width * originalProportions);
		switch (location) {
			case eTopLeft:
				destRect.x = padding;
				destRect.y = padding;
				break;
			case eTopRight:
				destRect.x = dimension.width - destRect.width - padding;
				destRect.y = padding;
				break;
			case eBottomLeft:
				destRect.x = padding;
				destRect.y = dimension.height - destRect.height - padding;
				break;
			case eBottomRight:
				destRect.x = dimension.width - destRect.width - padding;
				destRect.y = dimension.height - destRect.height - padding;
				break;	
			default:
				throw new Exception("Illegal location.");
		}
		Graphics2D graphics2D = (Graphics2D) image.getGraphics().create();
	    graphics2D.setColor(new Color(1.0f, 1.0f, 1.0f, 0.5f));
	    Rectangle roundedRect = new Rectangle(destRect.x - (padding/2), destRect.y - (padding/2), destRect.width + padding, destRect.height + padding);
	    graphics2D.fillRoundRect(roundedRect.x, roundedRect.y, roundedRect.width, roundedRect.height, 15, 15);
	    GraphicsEnvironment.getLocalGraphicsEnvironment();
		if (!GraphicsEnvironment.isHeadless()) {
	    	graphics2D.drawImage(BufferedImageUtils.generateShadow(logo, 5, Color.BLACK, 0.4f), destRect.x + 5, destRect.y + 5, destRect.width, destRect.height, null);
	    }
	    graphics2D.drawImage(logo, destRect.x, destRect.y, destRect.width, destRect.height, null);
	    graphics2D.dispose();
	    return roundedRect;
	}
	
	public EveryListingLogoSuperposer(File logoFile, ELocation location, int size, int padding) throws IOException {
		this.location = location;
		this.size = size;
		this.padding = padding;
		logo = ImageIO.read(logoFile);
	}
	
	@Override
	public ELocation getLocation() {
		return location;
	}
}
