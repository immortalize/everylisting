set url=http://everylisting.localhost
set /p url=Url [default is %url% if left empty]?: 

@echo off
set /P N=Enter parameter for merge ['merge' for prod and 'notmerge' for dev/qa]: 

:switch-case-example
  :: Call and mask out invalid call targets
  goto :switch-case-N-%N% 2>nul || (
    :: Default case
    echo Nothing to do...
  )
  goto :switch-case-end
  
  :switch-case-N-merge
    xcopy /y "importWithData.sql" "import.changed.sql"
    goto :switch-case-end     

  :switch-case-N-notmerge
    xcopy /y "import.sql" "import.changed.sql"
    goto :switch-case-end

:switch-case-end

"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "http://everylisting.localhost" %url%
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "http://dev.every-listing.com" %url%
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "https://dev.every-listing.com" %url%
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/themes/inspiry-real-places/images/register-icon.svg" "/mediacode/images/register2.png"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/themes/inspiry-real-places/images/fill-details-icon.svg" "/mediacode/images/clipboard.png"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/themes/inspiry-real-places/images/done-icon.svg" "/mediacode/images/done.png"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/logoalone_black_on_white_lg.jpg" "/mediacode/logos/logoalone/logoalone_black_on_white_lg.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/Courtney.jpg" "/mediacode/images/Courtney.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/CustomClient.jpg" "/wp-content/uploads/2020/08/Courtney.jpg" "/mediacode/images/CustomClient.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2019/10/silhouette-of-a-man-playing-golf-thumbnail-300x212-1.jpg" "/mediacode/images/silhouette-of-a-man-playing-golf-thumbnail-300x212.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2019/10/140222-M-TH981-002-300x198-1.jpg" "/mediacode/images/140222-M-TH981-002-300x198-1.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2019/10/sunset-woman-silhouette-sky-850x570-1.jpg" "/mediacode/images/sunset-woman-silhouette-sky-850x570-1.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/5.jpg" "/mediacode/logos/retired/5.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/Jane-Doe.jpg" "/mediacode/images/Jane-Doe.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/Janeth-Doe.jpg" "/mediacode/images/Janeth-Doe.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/Jim-Doe.jpg" "/mediacode/images/Jim-Doe.jpg"
"readMeMedia/fart.exe" -q -r -c -- "import.changed.sql" "/wp-content/uploads/2020/08/John-Doe.jpg" "/mediacode/images/John-Doe.jpg"



   VER > NUL # reset ERRORLEVEL
  GOTO :EOF # return from CALL

